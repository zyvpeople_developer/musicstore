package com.develop.zuzik.musicstore.datasource.cache

import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito

/**
 * User: zuzik
 * Date: 3/27/17
 */
class CacheQueryTest {

	val loadFromOriginalSource: () -> Single<String> = Mockito.mock(Function0::class.java) as () -> Single<String>
	val saveToCache: (String) -> Single<String> = Mockito.mock(Function1::class.java) as (String) -> Single<String>
	val loadFromCache: () -> Single<String> = Mockito.mock(Function0::class.java) as () -> Single<String>

	private val cacheQuery: CacheQuery<String> = CacheQuery<String>(loadFromOriginalSource, saveToCache, loadFromCache)

	@Before
	fun setUp() {
		Mockito.`when`(loadFromOriginalSource.invoke()).then { Single.just("original") }
		Mockito.`when`(saveToCache.invoke("original")).then { Single.just("saved") }
		Mockito.`when`(loadFromCache.invoke()).then { Single.just("cache") }
	}

	@Test
	fun loadLoadsFromOriginalAndSavesToCacheAndReturnsValueFromCache() {
		cacheQuery
				.load()
				.test()
				.assertValue("saved")
				.assertComplete()

		Mockito.verify(loadFromOriginalSource, Mockito.times(1)).invoke()
		Mockito.verify(saveToCache, Mockito.times(1)).invoke("original")
		Mockito.verifyZeroInteractions(loadFromCache)
	}

	@Test
	fun loadLoadsFromOriginalAndReturnsThisValueWhenSaveToCacheCausesError() {
		Mockito.`when`(saveToCache.invoke("original")).then { Single.error<String>(RuntimeException("error")) }

		cacheQuery
				.load()
				.test()
				.assertValue("original")
				.assertComplete()

		Mockito.verify(loadFromOriginalSource, Mockito.times(1)).invoke()
		Mockito.verify(saveToCache, Mockito.times(1)).invoke("original")
		Mockito.verifyZeroInteractions(loadFromCache)
	}

	@Test
	fun loadReturnsValueFromCacheWhenLoadFromOriginalCausesError() {
		Mockito.`when`(loadFromOriginalSource.invoke()).then { Single.error<String>(RuntimeException("error")) }

		cacheQuery
				.load()
				.test()
				.assertValue("cache")
				.assertComplete()

		Mockito.verify(loadFromOriginalSource, Mockito.times(1)).invoke()
		Mockito.verifyZeroInteractions(saveToCache)
		Mockito.verify(loadFromCache, Mockito.times(1)).invoke()
	}

	@Test
	fun loadReturnsErrorWhenLoadFromCacheCausesError() {
		val runtimeException = RuntimeException("cacheError")

		Mockito.`when`(loadFromOriginalSource.invoke()).then { Single.error<String>(RuntimeException("originalError")) }
		Mockito.`when`(loadFromCache.invoke()).then { Single.error<String>(runtimeException) }

		cacheQuery
				.load()
				.test()
				.assertError(runtimeException)

		Mockito.verify(loadFromOriginalSource, Mockito.times(1)).invoke()
		Mockito.verifyZeroInteractions(saveToCache)
		Mockito.verify(loadFromCache, Mockito.times(1)).invoke()
	}
}