package com.develop.zuzik.musicstore.presentation.extension

import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Marker
import io.reactivex.Observable
import io.reactivex.disposables.Disposable

/**
 * User: zuzik
 * Date: 3/26/17
 */

fun GoogleMap.markerClicks(): Observable<Marker> =
		Observable
				.create { emitter ->
					setOnMarkerClickListener { marker ->
						emitter.onNext(marker)
						true
					}
					emitter.setDisposable(object : Disposable {
						override fun isDisposed(): Boolean =
								false

						override fun dispose() {
							setOnMarkerClickListener(null)
						}
					})
				}