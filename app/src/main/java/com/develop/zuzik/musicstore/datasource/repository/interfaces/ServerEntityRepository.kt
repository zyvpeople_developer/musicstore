package com.develop.zuzik.musicstore.datasource.repository.interfaces

import com.develop.zuzik.musicstore.datasource.repository.exception.RepositoryException
import com.develop.zuzik.musicstore.domain.entity.ServerEntity

/**
 * User: zuzik
 * Date: 3/25/17
 */
interface ServerEntityRepository<E : ServerEntity> : EntityRepository<E> {

	@Throws(RepositoryException.Read::class)
	fun readWithSid(sid: Long): E

	@Throws(RepositoryException.Update::class)
	fun updateWithSid(entity: E): E

	@Throws(RepositoryException.Update::class)
	fun createOrUpdateWithSid(entity: E): E
}