package com.develop.zuzik.musicstore.domain.monad

/**
 * User: zuzik
 * Date: 3/26/17
 */
data class Optional<out Value>(val value: Value?)