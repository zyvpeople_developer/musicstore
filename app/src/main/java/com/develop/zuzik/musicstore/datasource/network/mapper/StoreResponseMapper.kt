package com.develop.zuzik.musicstore.datasource.network.mapper

import com.develop.zuzik.musicstore.datasource.network.response.StoreResponse
import com.develop.zuzik.musicstore.domain.entity.Store

/**
 * User: zuzik
 * Date: 3/25/17
 */
class StoreResponseMapper : ResponseMapper<Store, StoreResponse> {

	override fun toEntity(response: StoreResponse): Store =
			Store(
					id = 0,
					sid = response.id,
					name = response.name,
					address = response.address,
					phone = response.phone,
					//TODO: where to find correct email?
					//email = response.email,
					email = "zyvpeople@gmail.com",
					//TODO: where to find correct site?
					site = "http://www.ibanez.com",
					location = LocationResponseMapper().toEntity(response.location)
			)
}