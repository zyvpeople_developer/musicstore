package com.develop.zuzik.musicstore.datasource.repository.ormlite.mapper

/**
 * User: zuzik
 * Date: 3/25/17
 */
interface OrmliteEntityMapper<Entity, OrmliteEntity> {
	fun toEntity(ormliteEntity: OrmliteEntity): Entity
	fun toOrmliteEntity(entity: Entity): OrmliteEntity
}