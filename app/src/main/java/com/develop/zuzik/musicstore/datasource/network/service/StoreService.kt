package com.develop.zuzik.musicstore.datasource.network.service

import com.develop.zuzik.musicstore.datasource.network.response.StoreResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path


/**
 * User: zuzik
 * Date: 3/25/17
 */
interface StoreService {

	@GET("/stores/{storeId}")
	fun store(@Path("storeId") storeId: Long): Single<StoreResponse>
}