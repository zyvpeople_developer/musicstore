package com.develop.zuzik.musicstore.domain.store_navigation

import com.develop.zuzik.musicstore.domain.entity.Store
import com.develop.zuzik.musicstore.domain.redux.model.ReduxModel
import io.reactivex.Observable
import io.reactivex.Observer

/**
 * User: zuzik
 * Date: 3/26/17
 */
interface StoreNavigation {

	interface Model : ReduxModel<State> {
		val navigateToStore: Observer<Store>
		val clearStore: Observer<Any>
	}

	interface Presenter {
		fun onStart(view: View)
		fun onStop()
	}

	interface View {
		val navigateToStore: Observer<Store>

		val onStoreSelected: Observable<Store>
	}
}