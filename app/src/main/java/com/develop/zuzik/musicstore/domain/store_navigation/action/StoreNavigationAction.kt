package com.develop.zuzik.musicstore.domain.store_navigation.action

import com.develop.zuzik.musicstore.domain.entity.Store
import com.develop.zuzik.musicstore.domain.redux.Action

/**
 * User: zuzik
 * Date: 3/26/17
 */
sealed class StoreNavigationAction : Action {
	class NavigateToStore(val store: Store) : StoreNavigationAction()
	class ClearStore : StoreNavigationAction()
}