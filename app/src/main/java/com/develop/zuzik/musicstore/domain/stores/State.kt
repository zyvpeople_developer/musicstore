package com.develop.zuzik.musicstore.domain.stores

import com.develop.zuzik.musicstore.domain.entity.Store

/**
 * User: zuzik
 * Date: 3/24/17
 */
data class State(val stores: List<Store>,
				 val loading: Boolean)