package com.develop.zuzik.musicstore.datasource.repository.ormlite.entity

import com.j256.ormlite.field.DatabaseField
import com.j256.ormlite.table.DatabaseTable

/**
 * User: zuzik
 * Date: 3/25/17
 */
@DatabaseTable(tableName = StoreOrmliteEntity.TABLE)
class StoreOrmliteEntity : OrmliteServerEntity() {

	companion object Field {
		const val TABLE = "stores"
		const val NAME = "name"
		const val ADDRESS = "address"
		const val PHONE = "phone"
		const val EMAIL = "email"
		const val SITE = "site"
		const val LATITUDE = "latitude"
		const val LONGITUDE = "longitude"
	}

	@DatabaseField(columnName = NAME)
	var name: String = ""

	@DatabaseField(columnName = ADDRESS)
	var address: String = ""

	@DatabaseField(columnName = PHONE)
	var phone: String? = null

	@DatabaseField(columnName = EMAIL)
	var email: String = ""

	@DatabaseField(columnName = SITE)
	var site: String = ""

	@DatabaseField(columnName = LATITUDE)
	var latitude: Int = 0

	@DatabaseField(columnName = LONGITUDE)
	var longitude: Int = 0

}