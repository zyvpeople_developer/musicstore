package com.develop.zuzik.musicstore.datasource.repository.exception

/**
 * User: zuzik
 * Date: 3/25/17
 */
sealed class RepositoryException(message: String?) : Exception(message ?: "RepositoryException") {
	class Create : RepositoryException(null)
	class Read(message: String?) : RepositoryException(message)
	class Update : RepositoryException(null)
	class Delete : RepositoryException(null)
}