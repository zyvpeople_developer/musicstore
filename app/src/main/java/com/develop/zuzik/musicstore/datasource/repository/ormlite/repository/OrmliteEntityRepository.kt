package com.develop.zuzik.musicstore.datasource.repository.ormlite.repository

import com.develop.zuzik.musicstore.datasource.repository.exception.RepositoryException
import com.develop.zuzik.musicstore.datasource.repository.interfaces.EntityRepository
import com.develop.zuzik.musicstore.datasource.repository.ormlite.entity.OrmliteEntity
import com.develop.zuzik.musicstore.datasource.repository.ormlite.mapper.OrmliteEntityMapper
import com.develop.zuzik.musicstore.domain.entity.Entity
import com.j256.ormlite.dao.Dao

/**
 * User: zuzik
 * Date: 3/25/17
 */
open class OrmliteEntityRepository<E : Entity, OrmliteE : OrmliteEntity>(
		protected val dao: Dao<OrmliteE, Long>,
		protected val mapper: OrmliteEntityMapper<E, OrmliteE>,
		protected val setId: (Long, E) -> E) : EntityRepository<E> {

	@Throws(RepositoryException.Create::class)
	override fun create(entity: E): E {
		try {
			val ormliteEntity = mapper.toOrmliteEntity(entity)
			val createdOrmliteEntity = dao.createIfNotExists(ormliteEntity)
			return mapper.toEntity(createdOrmliteEntity)
		} catch (e: Exception) {
			throw RepositoryException.Create()
		}
	}

	@Throws(RepositoryException.Read::class)
	override fun readWithId(id: Long): E {
		try {
			val ormliteEntity = dao.queryForId(id) ?: throw RepositoryException.Read("Entity is not found")
			return mapper.toEntity(ormliteEntity)
		} catch (e: Exception) {
			throw RepositoryException.Read(e.message)
		}
	}

	@Throws(RepositoryException.Read::class)
	override fun readAll(): List<E> {
		try {
			return dao.queryForAll().map { mapper.toEntity(it) }
		} catch (e: Exception) {
			throw RepositoryException.Read(e.message)
		}
	}

	@Throws(RepositoryException.Update::class)
	override fun updateWithId(entity: E): E {
		try {
			val updatedEntitiesCount = dao.update(mapper.toOrmliteEntity(entity))
			return if (updatedEntitiesCount == 1) {
				entity
			} else {
				throw RepositoryException.Update()
			}
		} catch (e: Exception) {
			throw RepositoryException.Update()
		}
	}
}