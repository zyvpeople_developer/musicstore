package com.develop.zuzik.musicstore.domain.stores.query

import com.develop.zuzik.musicstore.domain.entity.Store
import io.reactivex.Single

/**
 * User: zuzik
 * Date: 3/24/17
 */
interface StoresQuery {
	fun load(): Single<List<Store>>
}