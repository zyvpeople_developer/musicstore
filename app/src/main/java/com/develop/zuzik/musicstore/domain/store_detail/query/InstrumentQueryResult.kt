package com.develop.zuzik.musicstore.domain.store_detail.query

import com.develop.zuzik.musicstore.domain.entity.Instrument
import com.develop.zuzik.musicstore.domain.entity.InstrumentStock

/**
 * User: zuzik
 * Date: 3/25/17
 */
data class InstrumentQueryResult(val instrument: Instrument,
								 val instrumentStock: InstrumentStock)