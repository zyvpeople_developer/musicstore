package com.develop.zuzik.musicstore.presentation.store_detail

import android.content.Context
import android.util.AttributeSet
import android.widget.LinearLayout
import com.develop.zuzik.musicstore.R
import kotlinx.android.synthetic.main.view_action.view.*


/**
 * User: zuzik
 * Date: 3/24/17
 */
class ActionView(context: Context?, attrs: AttributeSet?, defStyleAttr: Int)
	: LinearLayout(context, attrs, defStyleAttr) {

	constructor(context: Context?, attrs: AttributeSet?) : this(context, attrs, 0)
	constructor(context: Context?) : this(context, null)

	private val actionImageResId: Int
	private val actionText: String

	init {
		inflate(context, R.layout.view_action, this)
		val typedArray = context?.theme?.obtainStyledAttributes(attrs, R.styleable.ActionView, 0, 0)

		try {
			actionImageResId = typedArray?.getResourceId(R.styleable.ActionView_actionImage, 0) ?: 0
			actionText = typedArray?.getString(R.styleable.ActionView_actionText) ?: ""
		} finally {
			typedArray?.recycle()
		}
	}

	override fun onFinishInflate() {
		super.onFinishInflate()
		if (actionImageResId != 0) {
			image.setImageResource(actionImageResId)
		}
		text.text = actionText
	}
}