package com.develop.zuzik.musicstore.domain.store_detail.reducer

import com.develop.zuzik.musicstore.domain.redux.Action
import com.develop.zuzik.musicstore.domain.redux.Reducer
import com.develop.zuzik.musicstore.domain.store_detail.State
import com.develop.zuzik.musicstore.domain.store_detail.action.StoreDetailAction

/**
 * User: zuzik
 * Date: 3/24/17
 */
class StoreDetailReducer : Reducer<State> {
	override fun reduce(oldState: State, action: Action): State = (action as? StoreDetailAction)?.let {
		reduceStoreDetailAction(oldState, it)
	} ?: oldState

	private fun reduceStoreDetailAction(oldState: State, action: StoreDetailAction): State = when (action) {
		is StoreDetailAction.BeginLoad -> oldState.copy(loading = true)
		is StoreDetailAction.Load -> oldState.copy(
				loading = false,
				store = action.store,
				totalInstrumentsCount = action.instrumentQueryResults.map { it.instrumentStock.quantity }.sum(),
				instruments = action.instrumentQueryResults.map { it.instrument })
		is StoreDetailAction.Error -> oldState.copy(loading = false)
	}
}