package com.develop.zuzik.musicstore.presentation.mapper.image

import android.support.annotation.DrawableRes

/**
 * User: zuzik
 * Date: 3/26/17
 */
interface ImageResourceMapper<in Value> {
	@DrawableRes
	fun map(value: Value): Int
}