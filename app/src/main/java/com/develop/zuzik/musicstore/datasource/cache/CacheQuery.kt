package com.develop.zuzik.musicstore.datasource.cache

import io.reactivex.Single

/**
 * User: zuzik
 * Date: 3/24/17
 */
internal class CacheQuery<Value>(private val loadFromOriginalSource: () -> Single<Value>,
								 private val saveToCache: (Value) -> Single<Value>,
								 private val loadFromCache: () -> Single<Value>) {

	fun load(): Single<Value> =
			loadFromOriginalSource()
					.flatMap { value ->
						saveToCache(value)
								.onErrorReturn { value }
					}
					.onErrorResumeNext {
						loadFromCache()
					}
}