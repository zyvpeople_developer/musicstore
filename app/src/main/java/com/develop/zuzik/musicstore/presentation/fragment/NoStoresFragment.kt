package com.develop.zuzik.musicstore.presentation.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.develop.zuzik.musicstore.R

/**
 * User: zuzik
 * Date: 3/26/17
 */
class NoStoresFragment : Fragment() {

	override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? =
			inflater?.inflate(R.layout.fragment_no_stores, container, false)
}