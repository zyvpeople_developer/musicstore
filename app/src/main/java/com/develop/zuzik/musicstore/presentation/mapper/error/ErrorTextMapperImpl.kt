package com.develop.zuzik.musicstore.presentation.mapper.error

import android.content.Context
import com.develop.zuzik.musicstore.R

/**
 * User: zuzik
 * Date: 3/24/17
 */
class ErrorTextMapperImpl(private val context: Context) : ErrorTextMapper {

	//TODO: check error type and map to concrete message
	override fun map(error: Throwable): String =
			context.getString(R.string.error_unknown)
}