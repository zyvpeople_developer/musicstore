package com.develop.zuzik.musicstore.domain.location.location_provider

import android.location.Location
import io.reactivex.Single

/**
 * User: zuzik
 * Date: 3/26/17
 */
interface LocationProvider {
	fun location(): Single<Location>
}