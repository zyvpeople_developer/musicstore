package com.develop.zuzik.musicstore.presentation.store_detail

import android.content.Context
import android.util.AttributeSet
import android.widget.LinearLayout
import com.develop.zuzik.musicstore.R
import com.develop.zuzik.musicstore.domain.entity.Instrument
import com.develop.zuzik.musicstore.presentation.adapter.item.interfaces.ItemView
import com.develop.zuzik.musicstore.presentation.mapper.image.InstrumentTypeImageResourceMapper
import kotlinx.android.synthetic.main.view_instrument_item.view.*

/**
 * User: zuzik
 * Date: 3/24/17
 */
class InstrumentItemView(context: Context?, attrs: AttributeSet?, defStyleAttr: Int)
	: LinearLayout(context, attrs, defStyleAttr), ItemView<Instrument> {

	constructor(context: Context?, attrs: AttributeSet?) : this(context, attrs, 0)
	constructor(context: Context?) : this(context, null)

	private val imageResourceMapper = InstrumentTypeImageResourceMapper()

	init {
		inflate(context, R.layout.view_instrument_item, this)
	}

	override var item: Instrument? = null
		set(value) {
			tvBrand?.text = value?.brand
			tvModel?.text = value?.model
			ivType?.setImageResource(value?.type?.let { imageResourceMapper.map(it) } ?: android.R.color.transparent)
			tvPrice?.text = value?.price?.toString() ?: ""
			field = value
		}
}