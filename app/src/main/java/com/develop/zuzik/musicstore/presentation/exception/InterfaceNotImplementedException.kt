package com.develop.zuzik.musicstore.presentation.exception

/**
 * User: zuzik
 * Date: 3/24/17
 */
class InterfaceNotImplementedException(
		objectWithoutInterface: Any,
		interfaceForImplementation: Class<*>) : ClassCastException(
		"${interfaceForImplementation.simpleName} interface is not implemented in ${objectWithoutInterface.javaClass.simpleName} object")
