package com.develop.zuzik.musicstore.presentation.stores

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.develop.zuzik.musicstore.R
import com.develop.zuzik.musicstore.application.extension.app
import com.develop.zuzik.musicstore.domain.entity.Store
import com.develop.zuzik.musicstore.domain.stores.Stores
import com.develop.zuzik.musicstore.presentation.adapter.item.ItemAdapter
import com.develop.zuzik.musicstore.presentation.exception.InterfaceNotImplementedException
import com.develop.zuzik.musicstore.presentation.extension.toast
import com.develop.zuzik.musicstore.presentation.router.Router
import com.jakewharton.rxbinding2.support.v4.widget.refreshes
import com.jakewharton.rxbinding2.support.v4.widget.refreshing
import com.jakewharton.rxbinding2.view.clicks
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.fragment_stores.*

/**
 * User: zuzik
 * Date: 3/24/17
 */
class StoresFragment : Fragment() {

	companion object Factory {
		fun create() = StoresFragment()
	}

	private lateinit var router: Router
	private val adapter = ItemAdapter(StoreItemViewFactory())
	private var presenter: Stores.Presenter? = null
	private val compositeDisposable = CompositeDisposable()

	override fun onAttach(context: Context) {
		super.onAttach(context)
		router = context as? Router ?: throw InterfaceNotImplementedException(context, Router::class.java)
	}

	override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? =
			inflater?.inflate(R.layout.fragment_stores, container, false)

	override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)
		rvStores.layoutManager = LinearLayoutManager(context)
		rvStores.adapter = adapter
	}

	override fun onStart() {
		super.onStart()

		val view = object : Stores.View {
			override val displayStores: PublishSubject<List<Store>> = PublishSubject.create()
			override val displayProgress: PublishSubject<Boolean> = PublishSubject.create()
			override val displayError: PublishSubject<String> = PublishSubject.create()
			override val onRefresh: Observable<Unit> = swipeRefreshLayout.refreshes()
			override val onStoreClicked: Observable<Store> = adapter.itemClick
			override val onMapClicked: Observable<Unit> = btnMap.clicks()
		}

		compositeDisposable.addAll(
				view.displayStores.subscribe(adapter.update()),
				view.displayProgress.subscribe(swipeRefreshLayout.refreshing()),
				view.displayError.subscribe(context.toast())
		)

		presenter = StoresPresenter(
				context.app.injection.applicationComponent,
				router)
		presenter?.onStart(view)
	}

	override fun onStop() {
		presenter?.onStop()
		compositeDisposable.clear()
		super.onStop()
	}
}