package com.develop.zuzik.musicstore.application.injection.component

import com.develop.zuzik.musicstore.application.injection.module.ApplicationModule
import com.develop.zuzik.musicstore.application.injection.module.RepositoryModule
import com.develop.zuzik.musicstore.application.injection.module.StoreDetailModule
import com.develop.zuzik.musicstore.application.injection.module.query.QueryModule
import com.develop.zuzik.musicstore.presentation.map.presenter.StoreNavigationPresenter
import com.develop.zuzik.musicstore.presentation.stores.StoresPresenter
import dagger.Component
import javax.inject.Singleton

/**
 * User: zuzik
 * Date: 3/24/17
 */
@Singleton
@Component(modules = arrayOf(
		ApplicationModule::class,
		QueryModule::class,
		RepositoryModule::class))
interface ApplicationComponent {

	fun inject(presenter: StoresPresenter)

	fun storeDetailComponent(storeDetailModule: StoreDetailModule): StoreDetailComponent
	fun mapComponent(): MapComponent
}