package com.develop.zuzik.musicstore.domain.entity

/**
 * User: zuzik
 * Date: 3/24/17
 */
enum class InstrumentType {
	GUITAR,
	PIANO,
	UNKNOWN
}