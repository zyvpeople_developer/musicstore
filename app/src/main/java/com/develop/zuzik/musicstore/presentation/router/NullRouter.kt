package com.develop.zuzik.musicstore.presentation.router

import com.develop.zuzik.musicstore.domain.entity.Store

/**
 * User: zuzik
 * Date: 3/26/17
 */
class NullRouter : Router {

	override fun navigateToStoreDetailScreen(store: Store) {
	}

	override fun navigateToCallScreen(phone: String) {
	}

	override fun navigateToSendEmailScreen(email: String) {
	}

	override fun navigateToSiteScreen(site: String) {
	}

	override fun navigateToMapScreen() {
	}
}