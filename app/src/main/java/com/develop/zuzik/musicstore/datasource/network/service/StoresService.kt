package com.develop.zuzik.musicstore.datasource.network.service

import com.develop.zuzik.musicstore.datasource.network.response.StoreResponse
import io.reactivex.Single
import retrofit2.http.GET


/**
 * User: zuzik
 * Date: 3/25/17
 */
interface StoresService {

	@GET("/stores")
	fun stores(): Single<List<StoreResponse>>
}