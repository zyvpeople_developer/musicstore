package com.develop.zuzik.musicstore.presentation.map.presenter

import com.develop.zuzik.musicstore.application.injection.component.MapComponent
import com.develop.zuzik.musicstore.domain.store_navigation.State
import com.develop.zuzik.musicstore.domain.store_navigation.StoreNavigation
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

/**
 * User: zuzik
 * Date: 3/24/17
 */
class StoreNavigationPresenter(
		mapComponent: MapComponent) : StoreNavigation.Presenter {

	@Inject
	lateinit var model: StoreNavigation.Model

	private val compositeDisposable = CompositeDisposable()

	init {
		mapComponent.inject(this)
	}

	override fun onStart(view: StoreNavigation.View) {
		compositeDisposable.addAll(
				model
						.stateObservable
						.subscribe {
							updateView(view, it)
						},
				view
						.onStoreSelected
						.subscribe {
							model.navigateToStore.onNext(it)
						}
		)
	}

	override fun onStop() {
		compositeDisposable.clear()
	}

	private fun updateView(view: StoreNavigation.View, state: State) = when (state) {
		is State.InStore -> view.navigateToStore.onNext(state.store)
		is State.Nowhere -> {
		}
	}
}