package com.develop.zuzik.musicstore.presentation.extension

import android.content.Intent
import android.net.Uri


/**
 * User: zuzik
 * Date: 3/25/17
 */

fun Intent.sendEmail(email: String): Intent =
		setAction(Intent.ACTION_SENDTO)
				.setData(Uri.parse("mailto:"))
				.putExtra(Intent.EXTRA_EMAIL, arrayOf(email))

fun Intent.dialPhoneNumber(phoneNumber: String): Intent =
		setAction(Intent.ACTION_DIAL)
				.setData(Uri.parse("tel:" + phoneNumber))

fun Intent.openWebPage(url: String): Intent =
		setAction(Intent.ACTION_VIEW)
				.setData(Uri.parse(url))