package com.develop.zuzik.musicstore.datasource.fake

import com.develop.zuzik.musicstore.domain.entity.*

/**
 * User: zuzik
 * Date: 3/25/17
 */
internal object EntityFactory {

	fun createStore(id: Long): Store = Store(
			id = id,
			sid = id,
			name = "Name $id",
			address = "Address $id",
			phone = "Phone $id",
			email = "zyvpeople@gmail.com",
			site = "http://www.ibanez.com",
			location = Location(id.toInt(), id.toInt()))

	fun createInstrument(instrumentId: Long): Instrument = Instrument(
			id = instrumentId,
			sid = instrumentId,
			brand = "Brand $instrumentId",
			model = "Model $instrumentId",
			type = InstrumentType.GUITAR,
			price = instrumentId.toDouble(),
			storeId = 0)

	fun createInstrumentStock(instrumentId: Long): InstrumentStock = InstrumentStock(
			id = instrumentId,
			quantity = instrumentId.toInt(),
			instrumentId = instrumentId)
}