package com.develop.zuzik.musicstore.datasource.repository.ormlite.repository

import com.develop.zuzik.musicstore.datasource.repository.interfaces.InstrumentRepository
import com.develop.zuzik.musicstore.datasource.repository.ormlite.entity.InstrumentOrmliteEntity
import com.develop.zuzik.musicstore.datasource.repository.ormlite.mapper.InstrumentOrmliteEntityMapper
import com.develop.zuzik.musicstore.domain.entity.Instrument
import com.j256.ormlite.dao.Dao

/**
 * User: zuzik
 * Date: 3/25/17
 */
class InstrumentOrmliteRepository(
		dao: Dao<InstrumentOrmliteEntity, Long>) :
		OrmliteServerEntityRepository<Instrument, InstrumentOrmliteEntity>(
				dao,
				InstrumentOrmliteEntityMapper(),
				{ id, entity -> entity.copy(id = id) }),
		InstrumentRepository