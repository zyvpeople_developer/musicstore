package com.develop.zuzik.musicstore.presentation.extension

import com.develop.zuzik.musicstore.domain.entity.Location
import com.google.android.gms.maps.model.LatLng

/**
 * User: zuzik
 * Date: 3/26/17
 */

private const val locationCoef = 1e6

val Location.toLatLng: LatLng
	get() = LatLng(
			latitude / locationCoef,
			longitude / locationCoef)