package com.develop.zuzik.musicstore.application.injection.module

import android.content.Context
import com.develop.zuzik.musicstore.application.injection.scope.MapScope
import com.develop.zuzik.musicstore.domain.location.RequestLocation
import com.develop.zuzik.musicstore.domain.location.RequestLocationModel
import com.develop.zuzik.musicstore.domain.location.location_provider.GooglePlayServicesLocationProvider
import com.develop.zuzik.musicstore.domain.location.location_provider.LocationProvider
import com.develop.zuzik.musicstore.domain.store_navigation.StoreNavigation
import com.develop.zuzik.musicstore.domain.store_navigation.StoreNavigationModel
import com.develop.zuzik.musicstore.domain.stores.Stores
import com.google.android.gms.location.LocationRequest
import dagger.Module
import dagger.Provides

/**
 * User: zuzik
 * Date: 3/25/17
 */
@Module
class MapModule {

	@Provides
	@MapScope
	fun storeNavigationModel(storesModel: Stores.Model): StoreNavigation.Model = StoreNavigationModel(storesModel).apply { init() }

	@Provides
	@MapScope
	fun requestLocationModel(locationProvider: LocationProvider): RequestLocation.Model =
			RequestLocationModel(locationProvider)

	@Provides
	@MapScope
	fun locationProvider(context: Context): LocationProvider =
			GooglePlayServicesLocationProvider(
					context,
					LocationRequest()
							.setInterval(10000)
							.setFastestInterval(5000)
							.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY))
}