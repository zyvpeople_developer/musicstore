package com.develop.zuzik.musicstore.presentation.map.fragment

import android.content.Context
import android.util.AttributeSet
import android.widget.LinearLayout
import com.develop.zuzik.musicstore.R
import com.develop.zuzik.musicstore.domain.entity.Store
import com.develop.zuzik.musicstore.presentation.adapter.item.interfaces.ItemView
import kotlinx.android.synthetic.main.view_store_pager_item.view.*

/**
 * User: zuzik
 * Date: 3/24/17
 */
class StorePagerItemView(context: Context?, attrs: AttributeSet?, defStyleAttr: Int)
	: LinearLayout(context, attrs, defStyleAttr), ItemView<Store> {

	constructor(context: Context?, attrs: AttributeSet?) : this(context, attrs, 0)
	constructor(context: Context?) : this(context, null)

	init {
		inflate(context, R.layout.view_store_pager_item, this)
	}

	override var item: Store? = null
		set(value) {
			tvName?.text = value?.name
			tvAddress?.text = value?.address
			field = value
		}
}