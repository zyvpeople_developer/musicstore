package com.develop.zuzik.musicstore.datasource.repository.interfaces

import com.develop.zuzik.musicstore.domain.entity.Instrument

/**
 * User: zuzik
 * Date: 3/25/17
 */
interface InstrumentRepository : ServerEntityRepository<Instrument>