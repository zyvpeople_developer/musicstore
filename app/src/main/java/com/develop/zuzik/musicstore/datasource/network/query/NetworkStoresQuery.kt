package com.develop.zuzik.musicstore.datasource.network.query

import com.develop.zuzik.musicstore.datasource.network.factory.ServiceFactory
import com.develop.zuzik.musicstore.datasource.network.mapper.StoreResponseMapper
import com.develop.zuzik.musicstore.domain.entity.Store
import com.develop.zuzik.musicstore.domain.stores.query.StoresQuery
import io.reactivex.Scheduler
import io.reactivex.Single

/**
 * User: zuzik
 * Date: 3/25/17
 */
class NetworkStoresQuery(private val scheduler: Scheduler) : StoresQuery {

	private val responseMapper = StoreResponseMapper()

	override fun load(): Single<List<Store>> =
			ServiceFactory
					.createStoresService()
					.stores()
					.map { it.map { responseMapper.toEntity(it) } }
					.subscribeOn(scheduler)
}