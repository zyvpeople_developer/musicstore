package com.develop.zuzik.musicstore.domain.stores.reducer

import com.develop.zuzik.musicstore.domain.redux.Action
import com.develop.zuzik.musicstore.domain.redux.Reducer
import com.develop.zuzik.musicstore.domain.stores.State
import com.develop.zuzik.musicstore.domain.stores.action.StoresAction

/**
 * User: zuzik
 * Date: 3/24/17
 */
class StoresReducer : Reducer<State> {
	override fun reduce(oldState: State, action: Action): State = (action as? StoresAction)?.let {
		reduceStoresAction(oldState, it)
	} ?: oldState

	private fun reduceStoresAction(oldState: State, action: StoresAction): State = when (action) {
		is StoresAction.BeginLoad -> oldState.copy(loading = true)
		is StoresAction.Load -> oldState.copy(loading = false, stores = action.stores)
		is StoresAction.Error -> oldState.copy(loading = false)
	}
}