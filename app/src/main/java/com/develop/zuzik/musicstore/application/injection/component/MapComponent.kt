package com.develop.zuzik.musicstore.application.injection.component

import com.develop.zuzik.musicstore.application.injection.module.MapModule
import com.develop.zuzik.musicstore.application.injection.scope.MapScope
import com.develop.zuzik.musicstore.domain.location.RequestLocation
import com.develop.zuzik.musicstore.domain.store_navigation.StoreNavigation
import com.develop.zuzik.musicstore.presentation.map.presenter.RequestLocationPresenter
import com.develop.zuzik.musicstore.presentation.map.presenter.StoreNavigationPresenter
import dagger.Subcomponent

/**
 * User: zuzik
 * Date: 3/25/17
 */
@Subcomponent(modules = arrayOf(MapModule::class))
@MapScope
interface MapComponent {

	fun storeNavigationModel(): StoreNavigation.Model
	fun requestLocationModel(): RequestLocation.Model

	fun inject(presenter: StoreNavigationPresenter)
	fun inject(presenter: RequestLocationPresenter)
}