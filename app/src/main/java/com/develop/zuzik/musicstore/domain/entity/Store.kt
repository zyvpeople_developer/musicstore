package com.develop.zuzik.musicstore.domain.entity

/**
 * User: zuzik
 * Date: 3/24/17
 */
data class Store(
		override val id: Long,
		override val sid: Long,
		val name: String,
		val address: String,
		val phone: String?,
		val email: String,
		val site: String,
		val location: Location) : ServerEntity