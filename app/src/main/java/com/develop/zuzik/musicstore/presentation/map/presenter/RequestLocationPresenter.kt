package com.develop.zuzik.musicstore.presentation.map.presenter

import com.develop.zuzik.musicstore.application.injection.component.MapComponent
import com.develop.zuzik.musicstore.domain.location.RequestLocation
import com.develop.zuzik.musicstore.domain.location.location_provider.exception.LocationException
import com.develop.zuzik.musicstore.presentation.mapper.error.ErrorTextMapper
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

/**
 * User: zuzik
 * Date: 3/24/17
 */
class RequestLocationPresenter(mapComponent: MapComponent) : RequestLocation.Presenter {

	@Inject
	lateinit var model: RequestLocation.Model
	@Inject
	lateinit var errorTextMapper: ErrorTextMapper

	private val compositeDisposable = CompositeDisposable()

	init {
		mapComponent.inject(this)
	}

	override fun onStart(view: RequestLocation.View) {
		compositeDisposable.addAll(
				model
						.stateObservable
						.subscribe {
							if (it.location != null) {
								view.navigateToLocation.onNext(it.location)
							} else {
								view.clearLocation.onNext(Any())
							}
						},
				model
						.errorObservable
						.subscribe {
							if (it is LocationException.LocationPermission) {
								view.askLocationPermission.onNext(Any())
							} else {
								view.displayError.onNext(errorTextMapper.map(it))
							}
						},
				Observable.merge(
						view.onNavigateToLocation,
						view.onLocationPermissionGranted)
						.subscribe {
							model.requestLocation.onNext(Any())
						}
		)
	}

	override fun onStop() {
		compositeDisposable.clear()
	}
}