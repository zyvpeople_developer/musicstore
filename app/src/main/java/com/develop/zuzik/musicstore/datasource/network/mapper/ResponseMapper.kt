package com.develop.zuzik.musicstore.datasource.network.mapper

/**
 * User: zuzik
 * Date: 3/25/17
 */
interface ResponseMapper<out Entity, in Response> {
	fun toEntity(response: Response): Entity
}