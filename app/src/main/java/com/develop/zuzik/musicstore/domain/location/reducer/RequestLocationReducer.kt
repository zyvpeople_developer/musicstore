package com.develop.zuzik.musicstore.domain.location.reducer

import com.develop.zuzik.musicstore.domain.location.State
import com.develop.zuzik.musicstore.domain.location.action.RequestLocationAction
import com.develop.zuzik.musicstore.domain.redux.Action
import com.develop.zuzik.musicstore.domain.redux.Reducer

/**
 * User: zuzik
 * Date: 3/26/17
 */
class RequestLocationReducer : Reducer<State> {

	override fun reduce(oldState: State, action: Action): State = (action as? RequestLocationAction)?.let {
		reduceRequestLocationAction(oldState, it)
	} ?: oldState

	private fun reduceRequestLocationAction(oldState: State, action: RequestLocationAction): State = when (action) {
		is RequestLocationAction.UpdateLocation -> oldState.copy(action.location)
		is RequestLocationAction.Error -> oldState.copy(null)
	}
}