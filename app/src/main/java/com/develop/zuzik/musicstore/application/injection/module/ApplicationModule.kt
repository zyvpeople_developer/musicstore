package com.develop.zuzik.musicstore.application.injection.module

import android.content.Context
import com.develop.zuzik.musicstore.domain.stores.Stores
import com.develop.zuzik.musicstore.domain.stores.StoresModel
import com.develop.zuzik.musicstore.domain.stores.query.StoresQuery
import com.develop.zuzik.musicstore.presentation.mapper.error.ErrorTextMapper
import com.develop.zuzik.musicstore.presentation.mapper.error.ErrorTextMapperImpl
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers
import javax.inject.Singleton

/**
 * User: zuzik
 * Date: 3/24/17
 */
@Module
class ApplicationModule(private val context: Context) {

	@Provides
	fun context() = context

	@Provides
	fun errorTextMapper(context: Context): ErrorTextMapper = ErrorTextMapperImpl(context)

	@Provides
	@Singleton
	fun storesModel(storesQuery: StoresQuery): Stores.Model = StoresModel(storesQuery).apply { init() }

	@Provides
	@Singleton
	fun networkScheduler(): Scheduler = Schedulers.io()
}