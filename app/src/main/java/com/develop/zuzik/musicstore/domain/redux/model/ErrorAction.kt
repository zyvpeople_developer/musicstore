package com.develop.zuzik.musicstore.domain.redux.model

import com.develop.zuzik.musicstore.domain.redux.Action

/**
 * User: zuzik
 * Date: 3/24/17
 */
interface ErrorAction : Action {
	val error: Throwable
}