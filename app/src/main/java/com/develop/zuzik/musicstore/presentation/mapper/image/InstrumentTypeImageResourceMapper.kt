package com.develop.zuzik.musicstore.presentation.mapper.image

import com.develop.zuzik.musicstore.R
import com.develop.zuzik.musicstore.domain.entity.InstrumentType

/**
 * User: zuzik
 * Date: 3/26/17
 */
class InstrumentTypeImageResourceMapper : ImageResourceMapper<InstrumentType> {

	override fun map(value: InstrumentType): Int = when (value) {
		InstrumentType.GUITAR -> R.drawable.ic_guitar_acoustic
		InstrumentType.PIANO -> R.drawable.ic_piano
		InstrumentType.UNKNOWN -> R.drawable.ic_help
	}
}