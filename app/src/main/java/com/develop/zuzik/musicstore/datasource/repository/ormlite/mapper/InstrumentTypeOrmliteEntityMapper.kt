package com.develop.zuzik.musicstore.datasource.repository.ormlite.mapper

import com.develop.zuzik.musicstore.domain.entity.InstrumentType

/**
 * User: zuzik
 * Date: 3/25/17
 */
class InstrumentTypeOrmliteEntityMapper : OrmliteEntityMapper<InstrumentType, String?> {

	private val mapping: Map<InstrumentType, String?> = InstrumentType
			.values()
			.map { it to mapToString(it) }
			.toMap()

	override fun toEntity(ormliteEntity: String?): InstrumentType =
			mapping
					.entries
					.firstOrNull { it.value == ormliteEntity }
					?.key
					?: InstrumentType.UNKNOWN

	override fun toOrmliteEntity(entity: InstrumentType): String? =
			mapping[entity]!!

	private fun mapToString(entity: InstrumentType): String = when (entity) {
		InstrumentType.GUITAR -> "GUITAR"
		InstrumentType.PIANO -> "PIANO"
		InstrumentType.UNKNOWN -> "UNKNOWN"
	}
}