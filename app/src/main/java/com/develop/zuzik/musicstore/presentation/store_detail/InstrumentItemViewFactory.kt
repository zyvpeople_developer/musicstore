package com.develop.zuzik.musicstore.presentation.store_detail

import android.content.Context
import com.develop.zuzik.musicstore.domain.entity.Instrument
import com.develop.zuzik.musicstore.presentation.adapter.item.interfaces.ItemViewFactory

/**
 * User: zuzik
 * Date: 3/24/17
 */
class InstrumentItemViewFactory : ItemViewFactory<Instrument> {
	override fun create(context: Context?) = InstrumentItemView(context)
}