package com.develop.zuzik.musicstore.domain.stores.action

import com.develop.zuzik.musicstore.domain.entity.Store
import com.develop.zuzik.musicstore.domain.redux.Action
import com.develop.zuzik.musicstore.domain.redux.model.ErrorAction

/**
 * User: zuzik
 * Date: 3/24/17
 */
sealed class StoresAction : Action {
	class BeginLoad : StoresAction()
	class Load(val stores: List<Store>) : StoresAction()
	class Error(override val error: Throwable) : StoresAction(), ErrorAction
}