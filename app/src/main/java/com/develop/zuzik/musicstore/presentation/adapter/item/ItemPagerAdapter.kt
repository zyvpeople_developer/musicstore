package com.develop.zuzik.musicstore.presentation.adapter.item

import android.support.v4.view.PagerAdapter
import android.view.View
import android.view.ViewGroup
import com.develop.zuzik.musicstore.presentation.adapter.item.interfaces.ItemViewFactory
import io.reactivex.Observable
import io.reactivex.functions.Consumer
import io.reactivex.subjects.PublishSubject

/**
 * User: zuzik
 * Date: 12.03.2016.
 */
class ItemPagerAdapter<Item>(private val itemViewFactory: ItemViewFactory<Item>) : PagerAdapter() {

	var items = emptyList<Item>()
	private val itemClickSubject: PublishSubject<Item> = PublishSubject.create()
	val itemClick: Observable<Item>
		get() = itemClickSubject

	override fun instantiateItem(container: ViewGroup, position: Int): Any {
		val view = itemViewFactory.create(container.context)
		view.item = items[position]
		container.addView(view as View)
		view.setOnClickListener {
			view.item?.let {
				itemClickSubject.onNext(it)
			}
		}
		return view
	}

	override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
		container.removeView(`object` as View)
	}

	override fun getCount(): Int = items.size

	override fun isViewFromObject(view: View, `object`: Any): Boolean {
		return view === `object`
	}

	fun update(): Consumer<List<Item>> = Consumer {
		items = it
		notifyDataSetChanged()
	}
}
