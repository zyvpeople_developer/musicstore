package com.develop.zuzik.musicstore.application.injection.module

import android.content.Context
import com.develop.zuzik.musicstore.datasource.repository.interfaces.InstrumentRepository
import com.develop.zuzik.musicstore.datasource.repository.interfaces.InstrumentStockRepository
import com.develop.zuzik.musicstore.datasource.repository.interfaces.StoreRepository
import com.develop.zuzik.musicstore.datasource.repository.ormlite.entity.InstrumentOrmliteEntity
import com.develop.zuzik.musicstore.datasource.repository.ormlite.entity.InstrumentStockOrmliteEntity
import com.develop.zuzik.musicstore.datasource.repository.ormlite.entity.StoreOrmliteEntity
import com.develop.zuzik.musicstore.datasource.repository.ormlite.helper.OrmliteHelper
import com.develop.zuzik.musicstore.datasource.repository.ormlite.repository.InstrumentOrmliteRepository
import com.develop.zuzik.musicstore.datasource.repository.ormlite.repository.InstrumentStockOrmliteRepository
import com.develop.zuzik.musicstore.datasource.repository.ormlite.repository.StoreOrmliteRepository
import com.j256.ormlite.android.apptools.OpenHelperManager
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * User: zuzik
 * Date: 3/25/17
 */
@Module
class RepositoryModule {

	@Provides
	@Singleton
	fun helper(context: Context): OrmLiteSqliteOpenHelper =
			OpenHelperManager.getHelper(context, OrmliteHelper::class.java)

	@Provides
	@Singleton
	fun storeRepository(helper: OrmLiteSqliteOpenHelper): StoreRepository =
			StoreOrmliteRepository(helper.getDao(StoreOrmliteEntity::class.java))

	@Provides
	@Singleton
	fun instrumentRepository(helper: OrmLiteSqliteOpenHelper): InstrumentRepository =
			InstrumentOrmliteRepository(helper.getDao(InstrumentOrmliteEntity::class.java))

	@Provides
	@Singleton
	fun instrumentStockRepository(helper: OrmLiteSqliteOpenHelper): InstrumentStockRepository =
			InstrumentStockOrmliteRepository(
					helper.getDao(InstrumentStockOrmliteEntity::class.java),
					helper.getDao(InstrumentOrmliteEntity::class.java))
}