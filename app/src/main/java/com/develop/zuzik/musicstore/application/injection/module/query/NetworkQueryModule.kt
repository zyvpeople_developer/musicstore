package com.develop.zuzik.musicstore.application.injection.module.query

import com.develop.zuzik.musicstore.datasource.network.query.NetworkInstrumentsQuery
import com.develop.zuzik.musicstore.datasource.network.query.NetworkStoreQuery
import com.develop.zuzik.musicstore.datasource.network.query.NetworkStoresQuery
import com.develop.zuzik.musicstore.datasource.repository.interfaces.InstrumentRepository
import com.develop.zuzik.musicstore.datasource.repository.interfaces.InstrumentStockRepository
import com.develop.zuzik.musicstore.datasource.repository.interfaces.StoreRepository
import com.develop.zuzik.musicstore.domain.store_detail.query.InstrumentsQuery
import com.develop.zuzik.musicstore.domain.store_detail.query.StoreQuery
import com.develop.zuzik.musicstore.domain.stores.query.StoresQuery
import io.reactivex.Scheduler

/**
 * User: zuzik
 * Date: 3/25/17
 */
class NetworkQueryModule : QueryModule() {

	override fun storesQuery(storeRepository: StoreRepository, scheduler: Scheduler): StoresQuery = NetworkStoresQuery(scheduler)

	override fun storeQuery(storeRepository: StoreRepository, scheduler: Scheduler): StoreQuery = NetworkStoreQuery(scheduler)

	override fun instrumentStocksQuery(
			storeRepository: StoreRepository,
			instrumentRepository: InstrumentRepository,
			instrumentStockRepository: InstrumentStockRepository,
			scheduler: Scheduler): InstrumentsQuery = NetworkInstrumentsQuery(scheduler)

}