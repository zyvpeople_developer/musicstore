package com.develop.zuzik.musicstore.datasource.fake

import com.develop.zuzik.musicstore.domain.store_detail.query.InstrumentQueryResult
import com.develop.zuzik.musicstore.domain.store_detail.query.InstrumentsQuery
import io.reactivex.Single
import java.util.concurrent.TimeUnit

/**
 * User: zuzik
 * Date: 3/25/17
 */
class FakeInstrumentsQuery : InstrumentsQuery {

	override fun load(storeSid: Long): Single<List<InstrumentQueryResult>> =
			Single
					.just((0..20L).map {
						InstrumentQueryResult(
								EntityFactory.createInstrument(it),
								EntityFactory.createInstrumentStock(it))
					})
					.delay(3, TimeUnit.SECONDS)
}