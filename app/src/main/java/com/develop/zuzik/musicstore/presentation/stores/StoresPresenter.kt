package com.develop.zuzik.musicstore.presentation.stores

import com.develop.zuzik.musicstore.application.injection.component.ApplicationComponent
import com.develop.zuzik.musicstore.domain.stores.Stores
import com.develop.zuzik.musicstore.presentation.mapper.error.ErrorTextMapper
import com.develop.zuzik.musicstore.presentation.router.Router
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

/**
 * User: zuzik
 * Date: 3/24/17
 */
class StoresPresenter(
		applicationComponent: ApplicationComponent,
		private val router: Router) : Stores.Presenter {

	@Inject
	lateinit var model: Stores.Model
	@Inject
	lateinit var errorTextMapper: ErrorTextMapper
	private val compositeDisposable = CompositeDisposable()

	init {
		applicationComponent.inject(this)
	}

	override fun onStart(view: Stores.View) {
		compositeDisposable.addAll(
				model
						.stateObservable
						.subscribe {
							view.displayProgress.onNext(it.loading)
							view.displayStores.onNext(it.stores)
						},
				model
						.errorObservable
						.map { errorTextMapper.map(it) }
						.subscribe {
							view.displayError.onNext(it)
						},
				view
						.onRefresh
						.subscribe {
							model.refresh.onNext(it)
						},
				view
						.onStoreClicked
						.subscribe {
							router.navigateToStoreDetailScreen(it)
						},
				view
						.onMapClicked
						.subscribe {
							router.navigateToMapScreen()
						}
		)
	}

	override fun onStop() {
		compositeDisposable.clear()
	}
}