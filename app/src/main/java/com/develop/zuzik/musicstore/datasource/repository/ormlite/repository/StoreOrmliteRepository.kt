package com.develop.zuzik.musicstore.datasource.repository.ormlite.repository

import com.develop.zuzik.musicstore.datasource.repository.interfaces.StoreRepository
import com.develop.zuzik.musicstore.datasource.repository.ormlite.entity.StoreOrmliteEntity
import com.develop.zuzik.musicstore.datasource.repository.ormlite.mapper.StoreOrmliteEntityMapper
import com.develop.zuzik.musicstore.domain.entity.Store
import com.j256.ormlite.dao.Dao

/**
 * User: zuzik
 * Date: 3/25/17
 */
class StoreOrmliteRepository(
		dao: Dao<StoreOrmliteEntity, Long>) :
		OrmliteServerEntityRepository<Store, StoreOrmliteEntity>(
				dao,
				StoreOrmliteEntityMapper(),
				{ id, entity -> entity.copy(id = id) }),
		StoreRepository