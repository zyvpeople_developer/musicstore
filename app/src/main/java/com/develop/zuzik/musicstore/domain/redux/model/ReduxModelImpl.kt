package com.develop.zuzik.musicstore.domain.redux.model

import android.util.Log
import com.develop.zuzik.musicstore.domain.redux.Action
import com.develop.zuzik.musicstore.domain.redux.Reducer
import com.develop.zuzik.musicstore.domain.redux.ReduxStore
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject

/**
 * User: zuzik
 * Date: 3/24/17
 */
abstract class ReduxModelImpl<State>(private val defaultState: State) : ReduxModel<State> {

	override val stateObservable: BehaviorSubject<State> = BehaviorSubject.createDefault(defaultState)
	override val errorObservable: PublishSubject<Throwable> = PublishSubject.create()

	private var actions: List<Observable<Action>> = listOf()
	private var reducers: List<Reducer<State>> = listOf()

	private var disposable: Disposable? = null

	override fun init() {
		disposable = ReduxStore(defaultState, actions, reducers)
				.bind()
				.doOnError { errorObservable.onNext(it) }
				.retry()
				.subscribe { stateObservable.onNext(it) }
	}

	override fun release() {
		disposable?.dispose()
	}

	protected fun addAction(action: Observable<Action>) {
		actions += action
				.doOnNext {
					(it as? ErrorAction)?.let {
						Log.d(javaClass.simpleName, it.error.toString())
						errorObservable.onNext(it.error)
					}
				}
	}

	protected fun addReducer(reducer: Reducer<State>) {
		reducers += reducer
	}
}