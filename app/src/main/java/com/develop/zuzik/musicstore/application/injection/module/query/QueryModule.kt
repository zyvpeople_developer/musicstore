package com.develop.zuzik.musicstore.application.injection.module.query

import com.develop.zuzik.musicstore.datasource.repository.interfaces.InstrumentRepository
import com.develop.zuzik.musicstore.datasource.repository.interfaces.InstrumentStockRepository
import com.develop.zuzik.musicstore.datasource.repository.interfaces.StoreRepository
import com.develop.zuzik.musicstore.domain.store_detail.query.InstrumentsQuery
import com.develop.zuzik.musicstore.domain.store_detail.query.StoreQuery
import com.develop.zuzik.musicstore.domain.stores.query.StoresQuery
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler

/**
 * User: zuzik
 * Date: 3/25/17
 */
@Module
open class QueryModule {

	@Provides
	open fun storesQuery(storeRepository: StoreRepository, scheduler: Scheduler): StoresQuery = TODO()

	@Provides
	open fun storeQuery(storeRepository: StoreRepository, scheduler: Scheduler): StoreQuery = TODO()

	@Provides
	open fun instrumentStocksQuery(
			storeRepository: StoreRepository,
			instrumentRepository: InstrumentRepository,
			instrumentStockRepository: InstrumentStockRepository,
			scheduler: Scheduler): InstrumentsQuery = TODO()
}