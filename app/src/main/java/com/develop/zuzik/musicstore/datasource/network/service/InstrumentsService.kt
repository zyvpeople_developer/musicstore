package com.develop.zuzik.musicstore.datasource.network.service

import com.develop.zuzik.musicstore.datasource.network.response.InstrumentStockResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path

/**
 * User: zuzik
 * Date: 3/25/17
 */
interface InstrumentsService {

	@GET("/stores/{storeId}/instruments")
	fun instruments(@Path("storeId") storeId: Long): Single<List<InstrumentStockResponse>>
}