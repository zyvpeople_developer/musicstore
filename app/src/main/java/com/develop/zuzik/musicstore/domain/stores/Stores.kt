package com.develop.zuzik.musicstore.domain.stores

import com.develop.zuzik.musicstore.domain.entity.Store
import com.develop.zuzik.musicstore.domain.redux.model.ReduxModel
import io.reactivex.Observable
import io.reactivex.Observer

/**
 * User: zuzik
 * Date: 3/24/17
 */
interface Stores {

	interface Model : ReduxModel<State> {
		val refresh: Observer<Any>
	}

	interface Presenter {
		fun onStart(view: View)
		fun onStop()
	}

	interface View {
		val displayStores: Observer<List<Store>>
		val displayProgress: Observer<Boolean>
		val displayError: Observer<String>

		val onRefresh: Observable<Unit>
		val onStoreClicked: Observable<Store>
		val onMapClicked: Observable<Unit>
	}
}