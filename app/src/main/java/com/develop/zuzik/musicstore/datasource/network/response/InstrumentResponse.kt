package com.develop.zuzik.musicstore.datasource.network.response

/**
 * User: zuzik
 * Date: 3/25/17
 */
data class InstrumentResponse(
		val id: Long,
		val brand: String,
		val model: String,
		val type: String,
		val price: Double)