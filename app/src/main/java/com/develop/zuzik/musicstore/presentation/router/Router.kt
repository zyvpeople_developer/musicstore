package com.develop.zuzik.musicstore.presentation.router

import com.develop.zuzik.musicstore.domain.entity.Store

/**
 * User: zuzik
 * Date: 3/25/17
 */
interface Router {
	fun navigateToStoreDetailScreen(store: Store)
	fun navigateToCallScreen(phone: String)
	fun navigateToSendEmailScreen(email: String)
	fun navigateToSiteScreen(site: String)
	fun navigateToMapScreen()
}