package com.develop.zuzik.musicstore.domain.location

import android.location.Location
import com.develop.zuzik.musicstore.domain.redux.model.ReduxModel
import io.reactivex.Observable
import io.reactivex.Observer

/**
 * User: zuzik
 * Date: 3/26/17
 */
interface RequestLocation {

	interface Model : ReduxModel<State> {
		val requestLocation: Observer<Any>
	}

	interface Presenter {
		fun onStart(view: View)
		fun onStop()
	}

	interface View {
		val navigateToLocation: Observer<Location>
		val clearLocation: Observer<Any>
		val displayError: Observer<String>
		val askLocationPermission: Observer<Any>

		val onNavigateToLocation: Observable<Any>
		val onLocationPermissionGranted: Observable<Any>
	}
}