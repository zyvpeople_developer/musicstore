package com.develop.zuzik.musicstore.presentation.stores

import android.content.Context
import android.util.AttributeSet
import android.widget.LinearLayout
import com.develop.zuzik.musicstore.R
import com.develop.zuzik.musicstore.domain.entity.Store
import com.develop.zuzik.musicstore.presentation.adapter.item.interfaces.ItemView
import kotlinx.android.synthetic.main.view_store_item.view.*

/**
 * User: zuzik
 * Date: 3/24/17
 */
class StoreItemView(context: Context?, attrs: AttributeSet?, defStyleAttr: Int)
	: LinearLayout(context, attrs, defStyleAttr), ItemView<Store> {

	constructor(context: Context?, attrs: AttributeSet?) : this(context, attrs, 0)
	constructor(context: Context?) : this(context, null)

	init {
		inflate(context, R.layout.view_store_item, this)
	}

	override var item: Store? = null
		set(value) {
			tvName?.text = value?.name
			field = value
		}
}