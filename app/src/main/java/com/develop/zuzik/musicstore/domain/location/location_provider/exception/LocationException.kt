package com.develop.zuzik.musicstore.domain.location.location_provider.exception

/**
 * User: zuzik
 * Date: 3/26/17
 */
sealed class LocationException(message: String? = "LocationException") : Exception(message) {
	class GetLocation : LocationException()
	class GpsNotAvailableLocation : LocationException()
	class InternetNotAvailableLocation : LocationException()
	class GooglePlayServicesNotAvailable : LocationException()
	class ConnectGooglePlayServices(message: String?) : LocationException(message)
	class LocationPermission : LocationException()
}