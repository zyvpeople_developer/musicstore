package com.develop.zuzik.musicstore.datasource.repository.interfaces

import com.develop.zuzik.musicstore.datasource.repository.exception.RepositoryException
import com.develop.zuzik.musicstore.domain.entity.InstrumentStock
import com.develop.zuzik.musicstore.domain.store_detail.query.InstrumentQueryResult

/**
 * User: zuzik
 * Date: 3/25/17
 */
interface InstrumentStockRepository : EntityRepository<InstrumentStock> {

	@Throws(RepositoryException.Update::class)
	fun createOrUpdateWithInstrumentId(entity: InstrumentStock): InstrumentStock

	@Throws(RepositoryException.Read::class)
	fun readInstrumentsForStore(storeId: Long): List<InstrumentQueryResult>
}