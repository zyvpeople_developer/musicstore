package com.develop.zuzik.musicstore.domain.redux

/**
 * User: zuzik
 * Date: 3/24/17
 */
class CompositeReducer<State>(private val reducers: List<Reducer<State>>) : Reducer<State> {
	override fun reduce(oldState: State, action: Action): State =
			reducers.fold(oldState) { state, reducer -> reducer.reduce(state, action) }
}