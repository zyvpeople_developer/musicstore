package com.develop.zuzik.musicstore.datasource.network.query

import com.develop.zuzik.musicstore.datasource.network.factory.ServiceFactory
import com.develop.zuzik.musicstore.datasource.network.mapper.InstrumentResponseMapper
import com.develop.zuzik.musicstore.datasource.network.mapper.InstrumentStockResponseMapper
import com.develop.zuzik.musicstore.domain.store_detail.query.InstrumentQueryResult
import com.develop.zuzik.musicstore.domain.store_detail.query.InstrumentsQuery
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers

/**
 * User: zuzik
 * Date: 3/25/17
 */
class NetworkInstrumentsQuery(private val scheduler: Scheduler) : InstrumentsQuery {

	private val instrumentResponseMapper = InstrumentResponseMapper()
	private val instrumentStockResponseMapper = InstrumentStockResponseMapper()

	override fun load(storeSid: Long): Single<List<InstrumentQueryResult>> =
			ServiceFactory
					.createInstrumentsService()
					.instruments(storeSid)
					.map {
						it.map {
							InstrumentQueryResult(
									instrumentResponseMapper.toEntity(it.instrument),
									instrumentStockResponseMapper.toEntity(it))
						}
					}
					.subscribeOn(scheduler)
}