package com.develop.zuzik.musicstore.datasource.repository.ormlite.mapper

import com.develop.zuzik.musicstore.datasource.repository.ormlite.entity.StoreOrmliteEntity
import com.develop.zuzik.musicstore.domain.entity.Location
import com.develop.zuzik.musicstore.domain.entity.Store

/**
 * User: zuzik
 * Date: 3/25/17
 */
class StoreOrmliteEntityMapper : OrmliteEntityMapper<Store, StoreOrmliteEntity> {

	override fun toEntity(ormliteEntity: StoreOrmliteEntity): Store =
			Store(
					id = ormliteEntity.id,
					sid = ormliteEntity.sid,
					name = ormliteEntity.name,
					address = ormliteEntity.address,
					phone = ormliteEntity.phone,
					email = ormliteEntity.email,
					site = ormliteEntity.site,
					location = Location(ormliteEntity.latitude, ormliteEntity.longitude)
			)

	override fun toOrmliteEntity(entity: Store): StoreOrmliteEntity =
			StoreOrmliteEntity().apply {
				id = entity.id
				sid = entity.sid
				name = entity.name
				address = entity.address
				phone = entity.phone
				email = entity.email
				site = entity.site
				latitude = entity.location.latitude
				longitude = entity.location.longitude
			}
}