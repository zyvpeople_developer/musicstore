package com.develop.zuzik.musicstore.datasource.repository.ormlite.entity

import com.j256.ormlite.field.DatabaseField
import com.j256.ormlite.table.DatabaseTable

/**
 * User: zuzik
 * Date: 3/25/17
 */
@DatabaseTable(tableName = InstrumentOrmliteEntity.TABLE)
class InstrumentOrmliteEntity : OrmliteServerEntity() {

	companion object Field {
		const val TABLE = "instruments"
		const val BRAND = "brand"
		const val MODEL = "model"
		const val TYPE = "type"
		const val PRICE = "price"
		const val STORE = "store"
	}

	@DatabaseField(columnName = BRAND)
	var brand: String = ""

	@DatabaseField(columnName = MODEL)
	var model: String = ""

	@DatabaseField(columnName = TYPE)
	var type: String? = null

	@DatabaseField(columnName = PRICE)
	var price: Double = 0.0

	@DatabaseField(columnName = STORE, foreign = true)
	var store: StoreOrmliteEntity = StoreOrmliteEntity()
}