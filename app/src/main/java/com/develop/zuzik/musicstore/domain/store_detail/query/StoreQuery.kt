package com.develop.zuzik.musicstore.domain.store_detail.query

import com.develop.zuzik.musicstore.domain.entity.Store
import io.reactivex.Single

/**
 * User: zuzik
 * Date: 3/25/17
 */
interface StoreQuery {
	fun load(storeSid: Long): Single<Store>
}