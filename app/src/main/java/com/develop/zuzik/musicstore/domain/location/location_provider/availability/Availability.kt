package com.develop.zuzik.musicstore.domain.location.location_provider.availability

/**
 * User: zuzik
 * Date: 3/26/17
 */
interface Availability {
	fun available(): Boolean
}