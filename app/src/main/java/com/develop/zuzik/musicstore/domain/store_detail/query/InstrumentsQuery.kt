package com.develop.zuzik.musicstore.domain.store_detail.query

import io.reactivex.Single

/**
 * User: zuzik
 * Date: 3/25/17
 */
interface InstrumentsQuery {
	fun load(storeSid: Long): Single<List<InstrumentQueryResult>>
}