package com.develop.zuzik.musicstore.datasource.cache

import com.develop.zuzik.musicstore.datasource.repository.extension.createOrUpdateWithSidObservable
import com.develop.zuzik.musicstore.datasource.repository.extension.readWithSidObservable
import com.develop.zuzik.musicstore.datasource.repository.interfaces.StoreRepository
import com.develop.zuzik.musicstore.domain.entity.Store
import com.develop.zuzik.musicstore.domain.store_detail.query.StoreQuery
import io.reactivex.Single

/**
 * User: zuzik
 * Date: 3/24/17
 */
class CacheStoreQuery(private val storeQuery: StoreQuery,
					  private val storeRepository: StoreRepository) : StoreQuery {

	override fun load(storeSid: Long): Single<Store> =
			CacheQuery(
					{ storeQuery.load(storeSid) },
					{ storeRepository.createOrUpdateWithSidObservable(it) },
					{ storeRepository.readWithSidObservable(storeSid) })
					.load()
}