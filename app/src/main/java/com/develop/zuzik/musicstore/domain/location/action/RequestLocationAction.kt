package com.develop.zuzik.musicstore.domain.location.action

import android.location.Location
import com.develop.zuzik.musicstore.domain.redux.Action
import com.develop.zuzik.musicstore.domain.redux.model.ErrorAction

/**
 * User: zuzik
 * Date: 3/26/17
 */
sealed class RequestLocationAction : Action {
	class UpdateLocation(val location: Location) : RequestLocationAction()
	class Error(override val error: Throwable) : RequestLocationAction(), ErrorAction
}