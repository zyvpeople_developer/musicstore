package com.develop.zuzik.musicstore.presentation.map.fragment

import android.content.Context
import com.develop.zuzik.musicstore.domain.entity.Store
import com.develop.zuzik.musicstore.presentation.adapter.item.interfaces.ItemViewFactory

/**
 * User: zuzik
 * Date: 3/24/17
 */
class StorePagerItemViewFactory : ItemViewFactory<Store> {
	override fun create(context: Context?) = StorePagerItemView(context)
}