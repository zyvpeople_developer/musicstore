package com.develop.zuzik.musicstore.datasource.repository.ormlite.mapper

import com.develop.zuzik.musicstore.datasource.repository.ormlite.entity.InstrumentOrmliteEntity
import com.develop.zuzik.musicstore.datasource.repository.ormlite.entity.StoreOrmliteEntity
import com.develop.zuzik.musicstore.domain.entity.Instrument

/**
 * User: zuzik
 * Date: 3/25/17
 */
class InstrumentOrmliteEntityMapper : OrmliteEntityMapper<Instrument, InstrumentOrmliteEntity> {

	private val instrumentTypeMapper = InstrumentTypeOrmliteEntityMapper()

	override fun toEntity(ormliteEntity: InstrumentOrmliteEntity): Instrument =
			Instrument(
					id = ormliteEntity.id,
					sid = ormliteEntity.sid,
					brand = ormliteEntity.brand,
					model = ormliteEntity.model,
					type = instrumentTypeMapper.toEntity(ormliteEntity.type),
					price = ormliteEntity.price,
					storeId = ormliteEntity.store.id)

	override fun toOrmliteEntity(entity: Instrument): InstrumentOrmliteEntity =
			InstrumentOrmliteEntity().apply {
				id = entity.id
				sid = entity.sid
				brand = entity.brand
				model = entity.model
				type = instrumentTypeMapper.toOrmliteEntity(entity.type)
				price = entity.price
				store = StoreOrmliteEntity().apply { id = entity.storeId }
			}
}