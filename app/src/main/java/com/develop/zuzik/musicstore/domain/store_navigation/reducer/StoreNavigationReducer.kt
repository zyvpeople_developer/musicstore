package com.develop.zuzik.musicstore.domain.store_navigation.reducer

import com.develop.zuzik.musicstore.domain.redux.Action
import com.develop.zuzik.musicstore.domain.redux.Reducer
import com.develop.zuzik.musicstore.domain.store_navigation.State
import com.develop.zuzik.musicstore.domain.store_navigation.action.StoreNavigationAction

/**
 * User: zuzik
 * Date: 3/26/17
 */
class StoreNavigationReducer : Reducer<State> {

	override fun reduce(oldState: State, action: Action): State = (action as? StoreNavigationAction)?.let {
		reduceStoreNavigationAction(oldState, it)
	} ?: oldState

	private fun reduceStoreNavigationAction(oldState: State, action: StoreNavigationAction): State = when (action) {
		is StoreNavigationAction.NavigateToStore -> State.InStore(action.store)
		is StoreNavigationAction.ClearStore -> State.Nowhere()
	}
}