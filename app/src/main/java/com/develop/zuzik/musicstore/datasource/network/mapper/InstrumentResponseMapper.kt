package com.develop.zuzik.musicstore.datasource.network.mapper

import com.develop.zuzik.musicstore.datasource.network.response.InstrumentResponse
import com.develop.zuzik.musicstore.domain.entity.Instrument

/**
 * User: zuzik
 * Date: 3/25/17
 */
class InstrumentResponseMapper : ResponseMapper<Instrument, InstrumentResponse> {

	override fun toEntity(response: InstrumentResponse): Instrument =
			Instrument(
					id = 0,
					sid = response.id,
					brand = response.brand,
					model = response.model,
					type = InstrumentTypeResponseMapper().toEntity(response.type),
					price = response.price,
					storeId = 0)
}