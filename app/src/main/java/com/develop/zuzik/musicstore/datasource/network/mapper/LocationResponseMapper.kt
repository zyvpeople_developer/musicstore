package com.develop.zuzik.musicstore.datasource.network.mapper

import com.develop.zuzik.musicstore.datasource.network.response.LocationResponse
import com.develop.zuzik.musicstore.domain.entity.Location

/**
 * User: zuzik
 * Date: 3/25/17
 */
class LocationResponseMapper : ResponseMapper<Location, LocationResponse> {

	override fun toEntity(response: LocationResponse): Location =
			Location(
					latitude = response.latitude,
					longitude = response.longitude)
}