package com.develop.zuzik.musicstore.datasource.network.factory

import com.develop.zuzik.musicstore.datasource.network.service.InstrumentsService
import com.develop.zuzik.musicstore.datasource.network.service.StoreService
import com.develop.zuzik.musicstore.datasource.network.service.StoresService
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * User: zuzik
 * Date: 3/25/17
 */
object ServiceFactory {

	private val API_BASE_URL = "http://aschoolapi.appspot.com/"

	private val retrofit = Retrofit
			.Builder()
			.baseUrl(API_BASE_URL)
			.addConverterFactory(GsonConverterFactory.create())
			.addCallAdapterFactory(RxJava2CallAdapterFactory.create())
			.client(OkHttpClient.Builder().build())
			.build()

	fun createStoresService(): StoresService =
			retrofit.create(StoresService::class.java)

	fun createStoreService(): StoreService =
			retrofit.create(StoreService::class.java)

	fun createInstrumentsService(): InstrumentsService =
			retrofit.create(InstrumentsService::class.java)
}