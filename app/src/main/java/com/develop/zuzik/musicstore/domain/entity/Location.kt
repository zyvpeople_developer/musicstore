package com.develop.zuzik.musicstore.domain.entity

/**
 * User: zuzik
 * Date: 3/24/17
 */
data class Location(val latitude: Int,
					val longitude: Int)