package com.develop.zuzik.musicstore.domain.location

import android.location.Location

/**
 * User: zuzik
 * Date: 3/26/17
 */
data class State(val location: Location?)