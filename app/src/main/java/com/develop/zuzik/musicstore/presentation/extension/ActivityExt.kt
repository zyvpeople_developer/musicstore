package com.develop.zuzik.musicstore.presentation.extension

import android.app.Activity
import android.content.Intent
import android.support.v4.app.ActivityCompat
import io.reactivex.Single


/**
 * User: zuzik
 * Date: 3/25/17
 */

fun Activity.showChooser(intent: Intent, onFail: () -> Unit) {
	if (intent.resolveActivity(packageManager) != null) {
		startActivity(intent)
	} else {
		onFail()
	}
}

fun Activity.requestPermissionsSingle(requestCode: Int, permissions: Array<String>): Single<Any> =
		Single
				.defer {
					ActivityCompat.requestPermissions(this,
							permissions,
							requestCode)
					Single.just(Any())
				}