package com.develop.zuzik.musicstore.presentation.adapter.item.interfaces

/**
 * User: zuzik
 * Date: 3/24/17
 */
interface ItemView<Item> {
    var item: Item?
}