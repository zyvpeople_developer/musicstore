package com.develop.zuzik.musicstore.datasource.repository.interfaces

import com.develop.zuzik.musicstore.datasource.repository.exception.RepositoryException
import com.develop.zuzik.musicstore.domain.entity.Entity

/**
 * User: zuzik
 * Date: 3/25/17
 */
interface EntityRepository<E : Entity> {

	@Throws(RepositoryException.Create::class)
	fun create(entity: E): E

	@Throws(RepositoryException.Read::class)
	fun readWithId(id: Long): E

	@Throws(RepositoryException.Read::class)
	fun readAll(): List<E>

	@Throws(RepositoryException.Update::class)
	fun updateWithId(entity: E): E
}