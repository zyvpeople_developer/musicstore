package com.develop.zuzik.musicstore.presentation.store_detail

import com.develop.zuzik.musicstore.application.injection.component.StoreDetailComponent
import com.develop.zuzik.musicstore.domain.store_detail.State
import com.develop.zuzik.musicstore.domain.store_detail.StoreDetail
import com.develop.zuzik.musicstore.presentation.mapper.error.ErrorTextMapper
import com.develop.zuzik.musicstore.presentation.router.Router
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.BiFunction
import javax.inject.Inject

/**
 * User: zuzik
 * Date: 4/24/17
 */
class StoreDetailPresenter(
		storeDetailComponent: StoreDetailComponent,
		private val router: Router) : StoreDetail.Presenter {

	@Inject
	lateinit var model: StoreDetail.Model
	@Inject
	lateinit var errorTextMapper: ErrorTextMapper
	private val compositeDisposable = CompositeDisposable()

	init {
		storeDetailComponent.inject(this)
	}

	override fun onStart(view: StoreDetail.View) {
		compositeDisposable.addAll(
				model
						.stateObservable
						.subscribe {
							view.displayStoreName.onNext(it.store?.name ?: "")
							view.displayStoreAddress.onNext(it.store?.address ?: "")
							view.displayTotalInstrumentsCount.onNext(it.totalInstrumentsCount)
							view.displayInstruments.onNext(it.instruments)
							view.displayProgress.onNext(it.loading)
							view.allowCall.onNext(it.store?.phone != null)
						},
				model
						.errorObservable
						.map { errorTextMapper.map(it) }
						.subscribe {
							view.displayError.onNext(it)
						},
				view
						.onRefresh
						.subscribe {
							model.refresh.onNext(it)
						},
				view
						.onCall
						.withLatestFrom<State, State>(
								model.stateObservable,
								BiFunction { call, state -> state })
						.subscribe {
							it.store?.let {
								it.phone?.let {
									router.navigateToCallScreen(it)
								}
							}
						},
				view
						.onSendEmail
						.withLatestFrom<State, State>(
								model.stateObservable,
								BiFunction { sendEmail, state -> state })
						.subscribe {
							it.store?.let {
								router.navigateToSendEmailScreen(it.email)
							}
						},
				view
						.onOpenSite
						.withLatestFrom<State, State>(
								model.stateObservable,
								BiFunction { openSite, state -> state })
						.subscribe {
							it.store?.let {
								router.navigateToSiteScreen(it.site)
							}
						}
		)
	}

	override fun onStop() {
		compositeDisposable.clear()
	}
}