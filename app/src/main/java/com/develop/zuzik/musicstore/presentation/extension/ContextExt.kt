package com.develop.zuzik.musicstore.presentation.extension

import android.content.Context
import android.support.annotation.StringRes
import android.widget.Toast
import io.reactivex.functions.Consumer

/**
 * User: zuzik
 * Date: 3/25/17
 */

fun Context.showToast(@StringRes messageRedId: Int) {
	Toast.makeText(this, messageRedId, Toast.LENGTH_SHORT).show()
}

fun Context.showToast(message: String) {
	Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}

fun Context.toast(): Consumer<String> = Consumer {
	showToast(it)
}