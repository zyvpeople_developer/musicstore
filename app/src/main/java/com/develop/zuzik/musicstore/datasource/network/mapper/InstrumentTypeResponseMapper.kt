package com.develop.zuzik.musicstore.datasource.network.mapper

import com.develop.zuzik.musicstore.domain.entity.InstrumentType

/**
 * User: zuzik
 * Date: 3/25/17
 */
class InstrumentTypeResponseMapper : ResponseMapper<InstrumentType, String> {

	override fun toEntity(response: String): InstrumentType = when (response) {
		"GUITAR" -> InstrumentType.GUITAR
		"PIANO" -> InstrumentType.PIANO
		else -> InstrumentType.UNKNOWN
	}
}