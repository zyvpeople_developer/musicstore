package com.develop.zuzik.musicstore.presentation.store_detail

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.develop.zuzik.musicstore.R
import com.develop.zuzik.musicstore.application.extension.app
import com.develop.zuzik.musicstore.application.injection.component.StoreDetailComponent
import com.develop.zuzik.musicstore.domain.entity.Instrument
import com.develop.zuzik.musicstore.domain.entity.Store
import com.develop.zuzik.musicstore.domain.store_detail.StoreDetail
import com.develop.zuzik.musicstore.presentation.adapter.item.ItemAdapter
import com.develop.zuzik.musicstore.presentation.exception.InterfaceNotImplementedException
import com.develop.zuzik.musicstore.presentation.extension.toast
import com.develop.zuzik.musicstore.presentation.router.Router
import com.jakewharton.rxbinding2.support.v7.widget.itemClicks
import com.jakewharton.rxbinding2.view.clicks
import com.jakewharton.rxbinding2.view.enabled
import com.jakewharton.rxbinding2.view.visibility
import com.jakewharton.rxbinding2.widget.text
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.fragment_store_detail.*
import kotlinx.android.synthetic.main.view_actions.*

/**
 * User: zuzik
 * Date: 3/24/17
 */
class StoreDetailFragment : Fragment() {

	companion object Factory {
		private val ARGUMENT_STORE_ID = "ARGUMENT_STORE_ID"

		fun create(store: Store) = StoreDetailFragment().apply {
			arguments = Bundle().apply {
				putLong(ARGUMENT_STORE_ID, store.sid)
			}
		}
	}

	private var router: Router? = null
	private var component: StoreDetailComponent? = null
	private var presenter: StoreDetail.Presenter? = null
	private val adapter = ItemAdapter(InstrumentItemViewFactory())
	private val compositeDisposable = CompositeDisposable()

	override fun onAttach(context: Context) {
		super.onAttach(context)
		router = context as? Router ?: throw InterfaceNotImplementedException(context, Router::class.java)
	}

	override fun onDetach() {
		router = null
		super.onDetach()
	}

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		component = context.app.injection.storeDetailComponent(arguments.getLong(ARGUMENT_STORE_ID))
	}

	override fun onDestroy() {
		if (!activity.isChangingConfigurations) {
			component?.let {
				context.app.injection.clearStoreDetailComponent(it)
			}
			component = null
		}
		super.onDestroy()
	}

	override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? =
			inflater?.inflate(R.layout.fragment_store_detail, container, false)

	override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)
		rvInstruments.layoutManager = LinearLayoutManager(context)
		rvInstruments.adapter = adapter
		toolbar.inflateMenu(R.menu.menu_store_detail)
	}

	override fun onStart() {
		super.onStart()

		val view = object : StoreDetail.View {
			override val displayStoreName: PublishSubject<String> = PublishSubject.create()
			override val displayStoreAddress: PublishSubject<String> = PublishSubject.create()
			override val displayTotalInstrumentsCount: PublishSubject<Int> = PublishSubject.create()
			override val displayInstruments: PublishSubject<List<Instrument>> = PublishSubject.create()
			override val displayProgress: PublishSubject<Boolean> = PublishSubject.create()
			override val displayError: PublishSubject<String> = PublishSubject.create()
			override val allowCall: PublishSubject<Boolean> = PublishSubject.create()
			override val onRefresh: Observable<Any> = toolbar.itemClicks().filter { it.itemId == R.id.menuItemRefresh }.map { Any() }
			override val onCall: Observable<Unit> = viewCall.clicks()
			override val onSendEmail: Observable<Unit> = viewSendEmail.clicks()
			override val onOpenSite: Observable<Unit> = viewOpenSite.clicks()
		}

		compositeDisposable.addAll(
				view.displayStoreName.subscribe(tvStoreName.text()),
				view.displayStoreAddress.subscribe(viewAddress.text()),
				view.displayTotalInstrumentsCount.map { "${context.getString(R.string.stock)} $it" }.subscribe(viewTotalInstrumentsCount.text()),
				view.displayInstruments.subscribe(adapter.update()),
				view.displayProgress.subscribe(progress.visibility()),
				view.displayError.subscribe(context.toast()),
				view.allowCall.subscribe(viewCall.enabled())
		)

		component?.let { component ->
			router?.let { router ->
				presenter = StoreDetailPresenter(
						component,
						router)
			}
		}

		presenter?.onStart(view)
	}

	override fun onStop() {
		presenter?.onStop()
		compositeDisposable.clear()
		super.onStop()
	}
}