package com.develop.zuzik.musicstore.domain.location

import com.develop.zuzik.musicstore.domain.location.action.RequestLocationAction
import com.develop.zuzik.musicstore.domain.location.location_provider.LocationProvider
import com.develop.zuzik.musicstore.domain.location.reducer.RequestLocationReducer
import com.develop.zuzik.musicstore.domain.redux.Action
import com.develop.zuzik.musicstore.domain.redux.model.ReduxModelImpl
import io.reactivex.subjects.PublishSubject

/**
 * User: zuzik
 * Date: 3/26/17
 */
class RequestLocationModel(private val locationProvider: LocationProvider) :
		ReduxModelImpl<State>(State(null)),
		RequestLocation.Model {

	override val requestLocation: PublishSubject<Any> = PublishSubject.create()

	init {
		addAction(requestLocation
				.switchMap {
					locationProvider
							.location()
							.map<Action> { RequestLocationAction.UpdateLocation(it) }
							.onErrorReturn { RequestLocationAction.Error(it) }
							.toObservable()
				})
		addReducer(RequestLocationReducer())
	}
}