package com.develop.zuzik.musicstore.datasource.repository.extension

import com.develop.zuzik.musicstore.datasource.repository.interfaces.EntityRepository
import com.develop.zuzik.musicstore.domain.entity.Entity
import io.reactivex.Single

/**
 * User: zuzik
 * Date: 3/25/17
 */

fun <E : Entity> EntityRepository<E>.createObservable(entity: E): Single<E> =
		Single.defer { Single.just(create(entity)) }

fun <E : Entity> EntityRepository<E>.readAllObservable(): Single<List<E>> =
		Single.defer { Single.just(readAll()) }