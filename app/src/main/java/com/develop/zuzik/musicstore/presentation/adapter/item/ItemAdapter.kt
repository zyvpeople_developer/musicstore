package com.develop.zuzik.musicstore.presentation.adapter.item

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.develop.zuzik.musicstore.presentation.adapter.item.interfaces.ItemView
import com.develop.zuzik.musicstore.presentation.adapter.item.interfaces.ItemViewFactory
import io.reactivex.Observable
import io.reactivex.functions.Consumer
import io.reactivex.subjects.PublishSubject

/**
 * User: zuzik
 * Date: 3/24/17
 */
open class ItemAdapter<Item>(
		private val itemViewFactory: ItemViewFactory<Item>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

	internal class ItemViewHolder<Item>(val view: ItemView<Item>) : RecyclerView.ViewHolder(view as? View) {
		init {
			(view as? View)?.layoutParams = RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT)
		}
	}

	var items = emptyList<Item>()
	private val itemClickSubject: PublishSubject<Item> = PublishSubject.create()
	val itemClick: Observable<Item>
		get() = itemClickSubject

	fun update(): Consumer<List<Item>> = Consumer {
		items = it
		notifyDataSetChanged()
	}

	override fun getItemCount(): Int = items.size

	override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RecyclerView.ViewHolder =
			ItemViewHolder(itemViewFactory.create(parent?.context))

	override fun onBindViewHolder(holder: RecyclerView.ViewHolder?, position: Int) {
		val item = items[position]
		val view = (holder as? ItemViewHolder<Item>)?.view
		view?.item = item
		if (item != null && view is View) {
			view.setOnClickListener { itemClickSubject.onNext(item) }
		}
	}
}