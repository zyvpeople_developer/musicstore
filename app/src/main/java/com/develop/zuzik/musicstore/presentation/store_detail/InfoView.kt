package com.develop.zuzik.musicstore.presentation.store_detail

import android.content.Context
import android.util.AttributeSet
import android.widget.LinearLayout
import com.develop.zuzik.musicstore.R
import io.reactivex.functions.Consumer
import kotlinx.android.synthetic.main.view_info.view.*


/**
 * User: zuzik
 * Date: 3/24/17
 */
class InfoView(context: Context?, attrs: AttributeSet?, defStyleAttr: Int)
	: LinearLayout(context, attrs, defStyleAttr) {

	constructor(context: Context?, attrs: AttributeSet?) : this(context, attrs, 0)
	constructor(context: Context?) : this(context, null)

	private val infoImageResId: Int
	private var infoText: String

	init {
		inflate(context, R.layout.view_info, this)
		val typedArray = context?.theme?.obtainStyledAttributes(attrs, R.styleable.InfoView, 0, 0)

		try {
			infoImageResId = typedArray?.getResourceId(R.styleable.InfoView_infoImage, 0) ?: 0
			infoText = typedArray?.getString(R.styleable.InfoView_infoText) ?: ""
		} finally {
			typedArray?.recycle()
		}
	}

	override fun onFinishInflate() {
		super.onFinishInflate()
		if (infoImageResId != 0) {
			image.setImageResource(infoImageResId)
		}
		text.text = infoText
	}

	fun text(): Consumer<String> = Consumer {
		infoText = it
		text.text = it
	}
}