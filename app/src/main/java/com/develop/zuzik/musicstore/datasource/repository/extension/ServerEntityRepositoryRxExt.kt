package com.develop.zuzik.musicstore.datasource.repository.extension

import com.develop.zuzik.musicstore.datasource.repository.interfaces.ServerEntityRepository
import com.develop.zuzik.musicstore.domain.entity.ServerEntity
import io.reactivex.Single

/**
 * User: zuzik
 * Date: 3/25/17
 */

fun <E : ServerEntity> ServerEntityRepository<E>.readWithSidObservable(sid: Long): Single<E> =
		Single.defer { Single.just(readWithSid(sid)) }

fun <E : ServerEntity> ServerEntityRepository<E>.createOrUpdateWithSidObservable(entity: E): Single<E> =
		Single.defer { Single.just(createOrUpdateWithSid(entity)) }