package com.develop.zuzik.musicstore.presentation.map.activity

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.develop.zuzik.musicstore.R
import com.develop.zuzik.musicstore.application.extension.app
import com.develop.zuzik.musicstore.application.injection.component.MapComponent
import com.develop.zuzik.musicstore.domain.entity.Store
import com.develop.zuzik.musicstore.domain.location.RequestLocation
import com.develop.zuzik.musicstore.domain.monad.Optional
import com.develop.zuzik.musicstore.domain.store_navigation.StoreNavigation
import com.develop.zuzik.musicstore.domain.stores.Stores
import com.develop.zuzik.musicstore.presentation.extension.markerClicks
import com.develop.zuzik.musicstore.presentation.extension.requestPermissionsSingle
import com.develop.zuzik.musicstore.presentation.extension.toLatLng
import com.develop.zuzik.musicstore.presentation.extension.toast
import com.develop.zuzik.musicstore.presentation.map.fragment.StoresPagerFragment
import com.develop.zuzik.musicstore.presentation.map.presenter.RequestLocationPresenter
import com.develop.zuzik.musicstore.presentation.map.presenter.StoreNavigationPresenter
import com.develop.zuzik.musicstore.presentation.router.NullRouter
import com.develop.zuzik.musicstore.presentation.stores.StoresPresenter
import com.google.android.gms.maps.CameraUpdate
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.jakewharton.rxbinding2.view.clicks
import com.jakewharton.rxbinding2.view.visibility
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.functions.BiFunction
import io.reactivex.functions.Function3
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.activity_map.*


/**
 * User: zuzik
 * Date: 3/26/17
 */
class MapActivity : AppCompatActivity() {

	companion object Factory {

		private const val LOCATION_PERMISSION_REQUEST_CODE = 1

		fun create(context: Context): Intent =
				Intent(context, MapActivity::class.java)
	}

	private var storesPresenter: Stores.Presenter? = null
	private var storeNavigationPresenter: StoreNavigation.Presenter? = null
	private var requestLocationPresenter: RequestLocation.Presenter? = null
	private var mapComponent: MapComponent? = null
	private val compositeDisposable = CompositeDisposable()
	private val storeMarkers = mutableMapOf<String, Store>()
	private val locationPermissionGranted: PublishSubject<Any> = PublishSubject.create()

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_map)
		mapComponent = app.injection.mapComponent()
		displayStoresPagerFragment()
	}

	override fun onDestroy() {
		mapComponent = null
		if (!isChangingConfigurations) {
			app.injection.clearMapComponent()
		}
		super.onDestroy()
	}

	private fun displayStoresPagerFragment() {
		if (supportFragmentManager.findFragmentById(R.id.pagerFragmentContainer) == null) {
			supportFragmentManager
					.beginTransaction()
					.add(R.id.pagerFragmentContainer, StoresPagerFragment.create())
					.commit()
		}
	}

	override fun onStart() {
		super.onStart()

		val storesView = object : Stores.View {
			override val displayStores: PublishSubject<List<Store>> = PublishSubject.create()
			override val displayProgress: PublishSubject<Boolean> = PublishSubject.create()
			override val displayError: PublishSubject<String> = PublishSubject.create()
			override val onRefresh: Observable<Unit> = Observable.never()
			override val onStoreClicked: Observable<Store> = Observable.never()
			override val onMapClicked: Observable<Unit> = Observable.never()
		}

		val storeNavigationView = object : StoreNavigation.View {
			override val navigateToStore: PublishSubject<Store> = PublishSubject.create()
			override val onStoreSelected: Observable<Store> = markerClicked()
		}

		val requestLocationView = object : RequestLocation.View {
			override val navigateToLocation: PublishSubject<Location> = PublishSubject.create()
			override val clearLocation: PublishSubject<Any> = PublishSubject.create()
			override val displayError: PublishSubject<String> = PublishSubject.create()
			override val askLocationPermission: PublishSubject<Any> = PublishSubject.create()
			override val onNavigateToLocation: Observable<Any> = btnMyLocation.clicks().map { Any() }
			override val onLocationPermissionGranted: Observable<Any> = locationPermissionGranted
		}

		compositeDisposable.addAll(
				drawMap(storesView.displayStores,
						Observable.merge(
								requestLocationView.navigateToLocation.map { Optional(it) },
								requestLocationView.clearLocation.map { Optional(null) })),
				navigateToLocation(listOf(
						storeNavigationView
								.navigateToStore
								.map { it.location.toLatLng },
						requestLocationView
								.navigateToLocation
								.map { LatLng(it.latitude, it.longitude) })),
				displayProgress(storesView.displayProgress),
				displayError(listOf(
						storesView.displayError,
						requestLocationView.displayError)),
				requestLocationView
						.askLocationPermission
						.flatMap { askLocationPermission() }
						.subscribe())

		storesPresenter = StoresPresenter(app.injection.applicationComponent, NullRouter())
		storeNavigationPresenter = mapComponent?.let(::StoreNavigationPresenter)
		requestLocationPresenter = mapComponent?.let(::RequestLocationPresenter)

		storesPresenter?.onStart(storesView)
		storeNavigationPresenter?.onStart(storeNavigationView)
		requestLocationPresenter?.onStart(requestLocationView)
	}

	override fun onStop() {
		storesPresenter?.onStop()
		storeNavigationPresenter?.onStop()
		requestLocationPresenter?.onStop()
		compositeDisposable.clear()
		super.onStop()
	}

	override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
		when (requestCode) {
			LOCATION_PERMISSION_REQUEST_CODE -> {
				if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
					locationPermissionGranted.onNext(Any())
				}
			}
			else -> super.onRequestPermissionsResult(requestCode, permissions, grantResults)
		}
	}

	private fun googleMap(): Single<GoogleMap> {
		val mapFragment = supportFragmentManager.findFragmentById(R.id.mapFragment) as SupportMapFragment
		return Single.create<GoogleMap> { emitter ->
			mapFragment.getMapAsync { googleMap ->
				emitter.onSuccess(googleMap)
			}
		}
	}

	private fun createStoreMarker(store: Store): MarkerOptions =
			MarkerOptions()
					.position(store.location.toLatLng)
					.title(store.name)

	private fun createUserMarker(location: Location): MarkerOptions =
			MarkerOptions()
					.position(LatLng(location.latitude, location.longitude))
					.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE))

	private fun drawMap(stores: Observable<List<Store>>, location: Observable<Optional<Location>>): Disposable =
			Observable
					.combineLatest<GoogleMap, List<Store>, Optional<Location>, Triple<GoogleMap, List<Store>, Optional<Location>>>(
							googleMap().toObservable(),
							stores,
							location,
							Function3 { map, stores, location -> Triple(map, stores, location) })
					.subscribe { (map, stores, location) ->
						map.clear()
						storeMarkers.clear()

						stores.forEach {
							val marker = map.addMarker(createStoreMarker(it))
							storeMarkers.put(marker.id, it)
						}
						location.value?.let {
							map.addMarker(createUserMarker(it))
						}
					}

	private fun markerClicked(): Observable<Store> =
			googleMap()
					.flatMapObservable { it.markerClicks() }
					.flatMap {
						storeMarkers[it.id]?.let {
							Observable.just(it)
						} ?: Observable.empty()
					}

	private fun displayError(errors: List<Observable<String>>): Disposable =
			Observable.merge(errors).subscribe(toast())

	private fun navigateToLocation(locations: List<Observable<LatLng>>): Disposable =
			Observable
					.combineLatest<GoogleMap, CameraUpdate, Pair<GoogleMap, CameraUpdate>>(
							googleMap().toObservable(),
							Observable
									.merge(locations)
									.map { CameraUpdateFactory.newLatLng(it) },
							BiFunction { map, cameraUpdate -> map to cameraUpdate })
					.subscribe { (map, cameraUpdate) ->
						map.animateCamera(cameraUpdate)
					}

	private fun displayProgress(shouldDisplayProgress: Observable<Boolean>): Disposable =
			shouldDisplayProgress.subscribe(progress.visibility())

	private fun askLocationPermission(): Observable<Any> =
			requestPermissionsSingle(
					LOCATION_PERMISSION_REQUEST_CODE,
					arrayOf(Manifest.permission.ACCESS_FINE_LOCATION))
					.toObservable()
}