package com.develop.zuzik.musicstore.datasource.repository.ormlite.entity

import com.j256.ormlite.field.DatabaseField

/**
 * User: zuzik
 * Date: 3/25/17
 */
open class OrmliteServerEntity : OrmliteEntity() {

	companion object Field {
		const val SID = "sid"
	}

	@DatabaseField
	var sid: Long = 0
}