package com.develop.zuzik.musicstore.application

import android.app.Application
import com.develop.zuzik.musicstore.application.injection.Injection

/**
 * User: zuzik
 * Date: 3/24/17
 */
//TODO: sort data in query and repository
class App : Application() {

	companion object {
		lateinit var INSTANCE: App
			private set
	}

	lateinit var injection: Injection
		private set

	override fun onCreate() {
		super.onCreate()
		INSTANCE = this
		initInjection()
	}

	private fun initInjection() {
		injection = Injection(this)
	}
}