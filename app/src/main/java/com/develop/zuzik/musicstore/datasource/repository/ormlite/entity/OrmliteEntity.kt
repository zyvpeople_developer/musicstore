package com.develop.zuzik.musicstore.datasource.repository.ormlite.entity

import com.j256.ormlite.field.DatabaseField

/**
 * User: zuzik
 * Date: 3/25/17
 */
open class OrmliteEntity {

	companion object Field {
		const val ID = "id"
	}

	@DatabaseField(generatedId = true)
	var id: Long = 0
}