package com.develop.zuzik.musicstore.datasource.repository.extension

import com.develop.zuzik.musicstore.datasource.repository.interfaces.InstrumentStockRepository
import com.develop.zuzik.musicstore.domain.entity.InstrumentStock
import com.develop.zuzik.musicstore.domain.store_detail.query.InstrumentQueryResult
import io.reactivex.Single

/**
 * User: zuzik
 * Date: 3/25/17
 */

fun InstrumentStockRepository.createOrUpdateWithInstrumentIdObservable(entity: InstrumentStock): Single<InstrumentStock> =
		Single.defer { Single.just(createOrUpdateWithInstrumentId(entity)) }

fun InstrumentStockRepository.readInstrumentsForStoreObservable(storeId: Long): Single<List<InstrumentQueryResult>> =
		Single.defer { Single.just(readInstrumentsForStore(storeId)) }