package com.develop.zuzik.musicstore.application.injection.module.query

import com.develop.zuzik.musicstore.datasource.fake.FakeInstrumentsQuery
import com.develop.zuzik.musicstore.datasource.fake.FakeStoreQuery
import com.develop.zuzik.musicstore.datasource.fake.FakeStoresQuery
import com.develop.zuzik.musicstore.datasource.repository.interfaces.InstrumentRepository
import com.develop.zuzik.musicstore.datasource.repository.interfaces.InstrumentStockRepository
import com.develop.zuzik.musicstore.datasource.repository.interfaces.StoreRepository
import com.develop.zuzik.musicstore.domain.store_detail.query.InstrumentsQuery
import com.develop.zuzik.musicstore.domain.store_detail.query.StoreQuery
import com.develop.zuzik.musicstore.domain.stores.query.StoresQuery
import io.reactivex.Scheduler

/**
 * User: zuzik
 * Date: 3/25/17
 */
class FakeQueryModule : QueryModule() {

	override fun storesQuery(storeRepository: StoreRepository, scheduler: Scheduler): StoresQuery = FakeStoresQuery()

	override fun storeQuery(storeRepository: StoreRepository, scheduler: Scheduler): StoreQuery = FakeStoreQuery()

	override fun instrumentStocksQuery(
			storeRepository: StoreRepository,
			instrumentRepository: InstrumentRepository,
			instrumentStockRepository: InstrumentStockRepository,
			scheduler: Scheduler): InstrumentsQuery = FakeInstrumentsQuery()

}