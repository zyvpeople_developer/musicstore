package com.develop.zuzik.musicstore.application.injection

import android.content.Context
import android.util.LongSparseArray
import com.develop.zuzik.musicstore.application.injection.component.ApplicationComponent
import com.develop.zuzik.musicstore.application.injection.component.DaggerApplicationComponent
import com.develop.zuzik.musicstore.application.injection.component.MapComponent
import com.develop.zuzik.musicstore.application.injection.component.StoreDetailComponent
import com.develop.zuzik.musicstore.application.injection.module.ApplicationModule
import com.develop.zuzik.musicstore.application.injection.module.StoreDetailModule
import com.develop.zuzik.musicstore.application.injection.module.query.CacheQueryModule
import com.develop.zuzik.musicstore.application.injection.module.query.NetworkQueryModule

/**
 * User: zuzik
 * Date: 3/24/17
 */
class Injection(context: Context) {

	val applicationComponent: ApplicationComponent = DaggerApplicationComponent
			.builder()
			.applicationModule(ApplicationModule(context))
			.queryModule(CacheQueryModule(NetworkQueryModule()))
//			.queryModule(FakeQueryModule())
			.build()

	private val storeDetailComponents: LongSparseArray<StoreDetailComponent> = LongSparseArray()
	private var mapComponent: MapComponent? = null

	fun storeDetailComponent(storeSid: Long): StoreDetailComponent {
		val existedComponent = storeDetailComponents[storeSid]
		if (existedComponent != null) {
			return existedComponent
		} else {
			val newComponent = applicationComponent.storeDetailComponent(StoreDetailModule(storeSid))
			storeDetailComponents.put(storeSid, newComponent)
			newComponent.storeDetailModel().init()
			return newComponent
		}
	}

	fun clearStoreDetailComponent(storeDetailComponent: StoreDetailComponent) {
		storeDetailComponent.storeDetailModel().release()
		storeDetailComponents.indexOfValue(storeDetailComponent).let { index ->
			if (index != -1) {
				storeDetailComponents.removeAt(index)
			}
		}
	}

	fun mapComponent(): MapComponent {
		if (mapComponent == null) {
			mapComponent = applicationComponent.mapComponent()
			mapComponent?.storeNavigationModel()?.init()
			mapComponent?.requestLocationModel()?.init()
		}
		return mapComponent!!
	}

	fun clearMapComponent() {
		mapComponent?.storeNavigationModel()?.release()
		mapComponent?.requestLocationModel()?.release()
		mapComponent = null
	}
}