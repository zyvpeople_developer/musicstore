package com.develop.zuzik.musicstore.datasource.network.query

import com.develop.zuzik.musicstore.datasource.network.factory.ServiceFactory
import com.develop.zuzik.musicstore.datasource.network.mapper.StoreResponseMapper
import com.develop.zuzik.musicstore.domain.entity.Store
import com.develop.zuzik.musicstore.domain.store_detail.query.StoreQuery
import io.reactivex.Scheduler
import io.reactivex.Single

/**
 * User: zuzik
 * Date: 3/25/17
 */
class NetworkStoreQuery(private val scheduler: Scheduler) : StoreQuery {

	private val responseMapper = StoreResponseMapper()

	override fun load(storeSid: Long): Single<Store> =
			ServiceFactory
					.createStoreService()
					.store(storeSid)
					.map { responseMapper.toEntity(it) }
					.subscribeOn(scheduler)
}