package com.develop.zuzik.musicstore.application.injection.component

import com.develop.zuzik.musicstore.application.injection.module.StoreDetailModule
import com.develop.zuzik.musicstore.application.injection.scope.StoreDetailScope
import com.develop.zuzik.musicstore.domain.store_detail.StoreDetail
import com.develop.zuzik.musicstore.presentation.store_detail.StoreDetailPresenter
import dagger.Subcomponent

/**
 * User: zuzik
 * Date: 3/25/17
 */
@Subcomponent(modules = arrayOf(StoreDetailModule::class))
@StoreDetailScope
interface StoreDetailComponent {

	fun storeDetailModel(): StoreDetail.Model

	fun inject(presenter: StoreDetailPresenter)
}