package com.develop.zuzik.musicstore.datasource.repository.ormlite.entity

import com.j256.ormlite.field.DatabaseField
import com.j256.ormlite.table.DatabaseTable

/**
 * User: zuzik
 * Date: 3/25/17
 */
@DatabaseTable(tableName = InstrumentStockOrmliteEntity.TABLE)
class InstrumentStockOrmliteEntity : OrmliteEntity() {

	companion object Field {
		const val TABLE = "instrumentStocks"
		const val QUANTITY = "quantity"
		const val INSTRUMENT = "instrument"
	}

	@DatabaseField(columnName = QUANTITY)
	var quantity: Int = 0

	@DatabaseField(columnName = INSTRUMENT, foreign = true)
	var instrument: InstrumentOrmliteEntity = InstrumentOrmliteEntity()
}