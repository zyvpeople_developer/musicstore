package com.develop.zuzik.musicstore.domain.store_navigation

import com.develop.zuzik.musicstore.domain.entity.Store

/**
 * User: zuzik
 * Date: 3/26/17
 */
sealed class State {
	class InStore(val store: Store) : State()
	class Nowhere : State()
}