package com.develop.zuzik.musicstore.datasource.fake

import com.develop.zuzik.musicstore.domain.entity.Store
import com.develop.zuzik.musicstore.domain.store_detail.query.StoreQuery
import io.reactivex.Single
import java.util.concurrent.TimeUnit

/**
 * User: zuzik
 * Date: 3/25/17
 */
class FakeStoreQuery : StoreQuery {

	override fun load(storeSid: Long): Single<Store> =
			Single
					.just(EntityFactory.createStore(storeSid))
					.delay(3, TimeUnit.SECONDS)
}