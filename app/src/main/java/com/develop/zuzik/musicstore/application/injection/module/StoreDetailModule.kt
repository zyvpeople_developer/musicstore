package com.develop.zuzik.musicstore.application.injection.module

import com.develop.zuzik.musicstore.application.injection.scope.StoreDetailScope
import com.develop.zuzik.musicstore.domain.store_detail.StoreDetail
import com.develop.zuzik.musicstore.domain.store_detail.StoreDetailModel
import com.develop.zuzik.musicstore.domain.store_detail.query.InstrumentsQuery
import com.develop.zuzik.musicstore.domain.store_detail.query.StoreQuery
import dagger.Module
import dagger.Provides

/**
 * User: zuzik
 * Date: 3/25/17
 */
@Module
class StoreDetailModule(private val storeSid: Long) {

	@Provides
	@StoreDetailScope
	fun storeDetailModel(storeQuery: StoreQuery,
						 instrumentsQuery: InstrumentsQuery): StoreDetail.Model =
			StoreDetailModel(storeSid, storeQuery, instrumentsQuery)
}