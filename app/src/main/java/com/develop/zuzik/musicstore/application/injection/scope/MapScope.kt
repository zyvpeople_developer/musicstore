package com.develop.zuzik.musicstore.application.injection.scope

import javax.inject.Scope

/**
 * User: zuzik
 * Date: 3/25/17
 */
@Scope
@MustBeDocumented
@Retention(AnnotationRetention.RUNTIME)
annotation class MapScope