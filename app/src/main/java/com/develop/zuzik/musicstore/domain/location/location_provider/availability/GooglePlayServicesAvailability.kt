package com.develop.zuzik.musicstore.domain.location.location_provider.availability

import android.content.Context
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability

/**
 * User: zuzik
 * Date: 3/26/17
 */
class GooglePlayServicesAvailability(private val context: Context) : Availability {

	override fun available() = GoogleApiAvailability
			.getInstance()
			.isGooglePlayServicesAvailable(context) == ConnectionResult.SUCCESS;

}