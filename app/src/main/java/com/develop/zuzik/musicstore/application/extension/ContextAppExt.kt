package com.develop.zuzik.musicstore.application.extension

import android.content.Context
import com.develop.zuzik.musicstore.application.App

/**
 * User: zuzik
 * Date: 3/24/17
 */
val Context.app: App
    get() = App.INSTANCE