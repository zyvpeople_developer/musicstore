package com.develop.zuzik.musicstore.domain.store_detail

import com.develop.zuzik.musicstore.domain.redux.Action
import com.develop.zuzik.musicstore.domain.redux.model.ReduxModelImpl
import com.develop.zuzik.musicstore.domain.store_detail.action.StoreDetailAction
import com.develop.zuzik.musicstore.domain.store_detail.query.InstrumentsQuery
import com.develop.zuzik.musicstore.domain.store_detail.query.StoreQuery
import com.develop.zuzik.musicstore.domain.store_detail.reducer.StoreDetailReducer
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.subjects.PublishSubject

/**
 * User: zuzik
 * Date: 3/25/17
 */
class StoreDetailModel(
		storeSid: Long,
		private val storeQuery: StoreQuery,
		private val instrumentsQuery: InstrumentsQuery) :
		StoreDetail.Model,
		ReduxModelImpl<State>(State(null, 0, emptyList(), false)) {

	override val refresh: PublishSubject<Any> = PublishSubject.create()

	init {
		val loadFirstTime = Observable.just(Any())
		addAction(
				Observable
						.merge(loadFirstTime, refresh)
						.switchMap { loadStoreDetailInfo(storeSid) })
		addReducer(StoreDetailReducer())
	}

	private fun loadStoreDetailInfo(storeSid: Long): Observable<Action> =
			storeQuery
					.load(storeSid)
					.flatMap { store ->
						instrumentsQuery
								.load(store.sid)
								.map<Action> { StoreDetailAction.Load(store, it) }
					}
					.toObservable()
					.startWith(StoreDetailAction.BeginLoad())
					.onErrorReturn { StoreDetailAction.Error(it) }
					.observeOn(AndroidSchedulers.mainThread())
}