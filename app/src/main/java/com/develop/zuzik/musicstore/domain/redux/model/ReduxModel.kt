package com.develop.zuzik.musicstore.domain.redux.model

import io.reactivex.Observable

/**
 * User: zuzik
 * Date: 3/24/17
 */
interface ReduxModel<State> {

	val stateObservable: Observable<State>
	val errorObservable: Observable<Throwable>

	fun init()
	fun release()
}