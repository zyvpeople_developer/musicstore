package com.develop.zuzik.musicstore.presentation.mapper.error

/**
 * User: zuzik
 * Date: 3/24/17
 */
interface ErrorTextMapper {
	fun map(error: Throwable): String
}