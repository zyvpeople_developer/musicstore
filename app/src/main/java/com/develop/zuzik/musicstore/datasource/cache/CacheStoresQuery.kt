package com.develop.zuzik.musicstore.datasource.cache

import com.develop.zuzik.musicstore.datasource.repository.extension.createOrUpdateWithSidObservable
import com.develop.zuzik.musicstore.datasource.repository.extension.readAllObservable
import com.develop.zuzik.musicstore.datasource.repository.interfaces.StoreRepository
import com.develop.zuzik.musicstore.domain.entity.Store
import com.develop.zuzik.musicstore.domain.stores.query.StoresQuery
import io.reactivex.Observable
import io.reactivex.Single

/**
 * User: zuzik
 * Date: 3/24/17
 */
class CacheStoresQuery(private val storesQuery: StoresQuery,
					   private val storeRepository: StoreRepository) : StoresQuery {

	override fun load(): Single<List<Store>> =
			CacheQuery(
					{ storesQuery.load() },
					{
						Observable
								.fromIterable(it)
								.flatMap { storeRepository.createOrUpdateWithSidObservable(it).toObservable() }
								.toList()
					},
					{ storeRepository.readAllObservable() })
					.load()
}