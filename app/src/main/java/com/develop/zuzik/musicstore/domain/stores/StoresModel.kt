package com.develop.zuzik.musicstore.domain.stores

import com.develop.zuzik.musicstore.domain.redux.Action
import com.develop.zuzik.musicstore.domain.redux.model.ReduxModelImpl
import com.develop.zuzik.musicstore.domain.stores.action.StoresAction
import com.develop.zuzik.musicstore.domain.stores.query.StoresQuery
import com.develop.zuzik.musicstore.domain.stores.reducer.StoresReducer
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.subjects.PublishSubject

/**
 * User: zuzik
 * Date: 3/24/17
 */
class StoresModel(private val storesQuery: StoresQuery) :
		Stores.Model,
		ReduxModelImpl<State>(State(emptyList(), false)) {

	override val refresh: PublishSubject<Any> = PublishSubject.create()

	init {
		val loadFirstTime = Observable.just(Any())
		addAction(
				Observable
						.merge(loadFirstTime, refresh)
						.switchMap { loadStores() })
		addReducer(StoresReducer())
	}

	private fun loadStores(): Observable<Action> =
			storesQuery
					.load()
					.toObservable()
					.map<Action> { StoresAction.Load(it) }
					.startWith(StoresAction.BeginLoad())
					.onErrorReturn { StoresAction.Error(it) }
					.observeOn(AndroidSchedulers.mainThread())
}