package com.develop.zuzik.musicstore.domain.store_navigation

import com.develop.zuzik.musicstore.domain.entity.Store
import com.develop.zuzik.musicstore.domain.redux.model.ReduxModelImpl
import com.develop.zuzik.musicstore.domain.store_navigation.action.StoreNavigationAction
import com.develop.zuzik.musicstore.domain.store_navigation.reducer.StoreNavigationReducer
import com.develop.zuzik.musicstore.domain.stores.Stores
import io.reactivex.subjects.PublishSubject

/**
 * User: zuzik
 * Date: 3/26/17
 */
class StoreNavigationModel(storesModel: Stores.Model) :
		ReduxModelImpl<State>(State.Nowhere()),
		StoreNavigation.Model {

	override val navigateToStore: PublishSubject<Store> = PublishSubject.create()
	override val clearStore: PublishSubject<Any> = PublishSubject.create()

	init {
		addAction(navigateToStore.map { StoreNavigationAction.NavigateToStore(it) })
		addAction(clearStore.map { StoreNavigationAction.ClearStore() })
		addAction(storesModel
				.stateObservable
				.map { it.stores }
				.distinctUntilChanged()
				.map {
					it.firstOrNull()?.let {
						StoreNavigationAction.NavigateToStore(it)
					} ?: StoreNavigationAction.ClearStore()
				})
		addReducer(StoreNavigationReducer())
	}
}