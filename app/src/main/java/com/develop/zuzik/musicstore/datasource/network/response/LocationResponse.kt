package com.develop.zuzik.musicstore.datasource.network.response

/**
 * User: zuzik
 * Date: 3/24/17
 */
data class LocationResponse(
		val latitude: Int,
		val longitude: Int)