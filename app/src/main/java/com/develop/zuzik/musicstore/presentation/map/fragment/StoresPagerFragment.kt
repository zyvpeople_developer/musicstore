package com.develop.zuzik.musicstore.presentation.map.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.develop.zuzik.musicstore.R
import com.develop.zuzik.musicstore.application.extension.app
import com.develop.zuzik.musicstore.domain.entity.Store
import com.develop.zuzik.musicstore.domain.store_navigation.StoreNavigation
import com.develop.zuzik.musicstore.domain.stores.Stores
import com.develop.zuzik.musicstore.presentation.adapter.item.ItemPagerAdapter
import com.develop.zuzik.musicstore.presentation.map.presenter.StoreNavigationPresenter
import com.develop.zuzik.musicstore.presentation.router.NullRouter
import com.develop.zuzik.musicstore.presentation.stores.StoresPresenter
import com.jakewharton.rxbinding2.support.v4.view.currentItem
import com.jakewharton.rxbinding2.support.v4.view.pageSelections
import com.jakewharton.rxbinding2.view.visibility
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.fragment_stores_pager.*

/**
 * User: zuzik
 * Date: 3/24/17
 */
class StoresPagerFragment : Fragment() {

	companion object Factory {
		fun create() = StoresPagerFragment()
	}

	private val adapter = ItemPagerAdapter(StorePagerItemViewFactory())
	private var storesPresenter: Stores.Presenter? = null
	private var storeNavigationPresenter: StoreNavigation.Presenter? = null
	private val compositeDisposable = CompositeDisposable()

	override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? =
			inflater?.inflate(R.layout.fragment_stores_pager, container, false)

	override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)
		viewPager.adapter = adapter
	}

	override fun onStart() {
		super.onStart()

		val storesView = object : Stores.View {
			override val displayStores: PublishSubject<List<Store>> = PublishSubject.create()
			override val displayProgress: PublishSubject<Boolean> = PublishSubject.create()
			override val displayError: PublishSubject<String> = PublishSubject.create()
			override val onRefresh: Observable<Unit> = Observable.never()
			override val onStoreClicked: Observable<Store> = Observable.never()
			override val onMapClicked: Observable<Unit> = Observable.never()
		}

		val storeNavigationView = object : StoreNavigation.View {
			override val navigateToStore: PublishSubject<Store> = PublishSubject.create()
			override val onStoreSelected: Observable<Store> =
					Observable.merge(
							viewPager
									.pageSelections()
									.flatMap
									{
										adapter.items.getOrNull(it)?.let {
											Observable.just(it)
										} ?: Observable.empty()
									},
							adapter.itemClick)

		}

		compositeDisposable.addAll(
				storesView
						.displayStores
						.subscribe(adapter.update()),
				storesView
						.displayStores
						.map { !it.isEmpty() }
						.subscribe(viewPager.visibility()),
				storesView
						.displayStores
						.map { it.isEmpty() }
						.subscribe(viewNoStores.visibility()),
				storeNavigationView
						.navigateToStore
						.map { adapter.items.indexOf(it) }
						.subscribe(viewPager.currentItem()))

		storesPresenter = StoresPresenter(context.app.injection.applicationComponent, NullRouter())
		storeNavigationPresenter = StoreNavigationPresenter(context.app.injection.mapComponent())

		storesPresenter?.onStart(storesView)
		storeNavigationPresenter?.onStart(storeNavigationView)
	}

	override fun onStop() {
		storesPresenter?.onStop()
		storeNavigationPresenter?.onStop()
		compositeDisposable.clear()
		super.onStop()
	}
}