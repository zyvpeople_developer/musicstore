package com.develop.zuzik.musicstore.application.injection.module.query

import com.develop.zuzik.musicstore.datasource.cache.CacheInstrumentsQuery
import com.develop.zuzik.musicstore.datasource.cache.CacheStoreQuery
import com.develop.zuzik.musicstore.datasource.cache.CacheStoresQuery
import com.develop.zuzik.musicstore.datasource.repository.interfaces.InstrumentRepository
import com.develop.zuzik.musicstore.datasource.repository.interfaces.InstrumentStockRepository
import com.develop.zuzik.musicstore.datasource.repository.interfaces.StoreRepository
import com.develop.zuzik.musicstore.domain.store_detail.query.InstrumentsQuery
import com.develop.zuzik.musicstore.domain.store_detail.query.StoreQuery
import com.develop.zuzik.musicstore.domain.stores.query.StoresQuery
import io.reactivex.Scheduler

/**
 * User: zuzik
 * Date: 3/25/17
 */
class CacheQueryModule(private val originalQueryModule: QueryModule) : QueryModule() {

	override fun storesQuery(storeRepository: StoreRepository, scheduler: Scheduler): StoresQuery =
			CacheStoresQuery(
					originalQueryModule.storesQuery(storeRepository, scheduler),
					storeRepository)

	override fun storeQuery(storeRepository: StoreRepository, scheduler: Scheduler): StoreQuery =
			CacheStoreQuery(
					originalQueryModule.storeQuery(storeRepository, scheduler),
					storeRepository)

	override fun instrumentStocksQuery(
			storeRepository: StoreRepository,
			instrumentRepository: InstrumentRepository,
			instrumentStockRepository: InstrumentStockRepository,
			scheduler: Scheduler): InstrumentsQuery =
			CacheInstrumentsQuery(
					originalQueryModule.instrumentStocksQuery(storeRepository, instrumentRepository, instrumentStockRepository, scheduler),
					storeRepository,
					instrumentRepository,
					instrumentStockRepository)


}