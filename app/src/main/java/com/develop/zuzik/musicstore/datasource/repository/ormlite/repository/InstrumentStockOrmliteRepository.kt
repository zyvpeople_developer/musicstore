package com.develop.zuzik.musicstore.datasource.repository.ormlite.repository

import com.develop.zuzik.musicstore.datasource.repository.exception.RepositoryException
import com.develop.zuzik.musicstore.datasource.repository.interfaces.InstrumentStockRepository
import com.develop.zuzik.musicstore.datasource.repository.ormlite.entity.InstrumentOrmliteEntity
import com.develop.zuzik.musicstore.datasource.repository.ormlite.entity.InstrumentStockOrmliteEntity
import com.develop.zuzik.musicstore.datasource.repository.ormlite.mapper.InstrumentOrmliteEntityMapper
import com.develop.zuzik.musicstore.datasource.repository.ormlite.mapper.InstrumentStockOrmliteEntityMapper
import com.develop.zuzik.musicstore.domain.entity.InstrumentStock
import com.develop.zuzik.musicstore.domain.store_detail.query.InstrumentQueryResult
import com.j256.ormlite.dao.Dao


/**
 * User: zuzik
 * Date: 3/25/17
 */
class InstrumentStockOrmliteRepository(
		dao: Dao<InstrumentStockOrmliteEntity, Long>,
		private val instrumentDao: Dao<InstrumentOrmliteEntity, Long>) :
		OrmliteEntityRepository<InstrumentStock, InstrumentStockOrmliteEntity>(
				dao,
				InstrumentStockOrmliteEntityMapper(),
				{ id, entity -> entity.copy(id = id) }),
		InstrumentStockRepository {

	@Throws(RepositoryException.Update::class)
	override fun createOrUpdateWithInstrumentId(entity: InstrumentStock): InstrumentStock =
			try {
				updateWithInstrumentId(entity)
			} catch (e: RepositoryException.Update) {
				try {
					create(entity)
				} catch (e: Exception) {
					throw RepositoryException.Update()
				}
			}

	@Throws(RepositoryException.Read::class)
	private fun readWithInstrumentId(instrumentId: Long): InstrumentStockOrmliteEntity =
			try {
				dao.queryForEq(InstrumentStockOrmliteEntity.INSTRUMENT, instrumentId).firstOrNull() ?: throw RepositoryException.Read("Entity is not found")
			} catch (e: Exception) {
				throw RepositoryException.Read(e.message)
			}

	@Throws(RepositoryException.Update::class)
	private fun updateWithInstrumentId(entity: InstrumentStock): InstrumentStock =
			try {
				val existedOrmliteEntity = readWithInstrumentId(entity.instrumentId)
				val entityToUpdateWithId = setId(existedOrmliteEntity.id, entity)
				updateWithId(entityToUpdateWithId)
			} catch (e: Exception) {
				throw RepositoryException.Update()
			}

	@Throws(RepositoryException.Read::class)
	override fun readInstrumentsForStore(storeId: Long): List<InstrumentQueryResult> =
			try {
				//FIXME: use raw query to retrieve all columns from both tables

				val instrumentMapper = InstrumentOrmliteEntityMapper()
				val instrumentQueryBuilder = instrumentDao.queryBuilder()
				instrumentQueryBuilder
						.where()
						.eq(InstrumentOrmliteEntity.STORE, storeId)
				val result = instrumentQueryBuilder
						.leftJoin(dao.queryBuilder())
						.query()

				result
						.map {
							InstrumentQueryResult(
									instrumentMapper.toEntity(it),
									InstrumentStock(0, 0, 0))
						}

			} catch (e: Exception) {
				throw RepositoryException.Read(e.message)
			}
}