package com.develop.zuzik.musicstore.datasource.fake

import com.develop.zuzik.musicstore.domain.entity.Store
import com.develop.zuzik.musicstore.domain.stores.query.StoresQuery
import io.reactivex.Single
import java.util.concurrent.TimeUnit

/**
 * User: zuzik
 * Date: 3/24/17
 */
class FakeStoresQuery : StoresQuery {

	override fun load(): Single<List<Store>> =
			Single
					.just((0..100L).map { EntityFactory.createStore(it) })
					.delay(3, TimeUnit.SECONDS)
}