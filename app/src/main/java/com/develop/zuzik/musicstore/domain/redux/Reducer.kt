package com.develop.zuzik.musicstore.domain.redux


/**
 * User: zuzik
 * Date: 3/24/17
 */
interface Reducer<State> {
	fun reduce(oldState: State, action: Action): State
}