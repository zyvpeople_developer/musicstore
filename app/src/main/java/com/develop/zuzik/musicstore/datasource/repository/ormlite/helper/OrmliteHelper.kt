package com.develop.zuzik.musicstore.datasource.repository.ormlite.helper

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import com.develop.zuzik.musicstore.datasource.repository.ormlite.entity.InstrumentOrmliteEntity
import com.develop.zuzik.musicstore.datasource.repository.ormlite.entity.InstrumentStockOrmliteEntity
import com.develop.zuzik.musicstore.datasource.repository.ormlite.entity.StoreOrmliteEntity
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper
import com.j256.ormlite.support.ConnectionSource
import com.j256.ormlite.table.TableUtils

/**
 * User: zuzik
 * Date: 3/25/17
 */
class OrmliteHelper(context: Context) : OrmLiteSqliteOpenHelper(context, "musicStoreDatabase", null, 1) {

	override fun onCreate(database: SQLiteDatabase?, connectionSource: ConnectionSource?) {
		TableUtils.createTable(connectionSource, StoreOrmliteEntity::class.java)
		TableUtils.createTable(connectionSource, InstrumentOrmliteEntity::class.java)
		TableUtils.createTable(connectionSource, InstrumentStockOrmliteEntity::class.java)
	}

	override fun onUpgrade(database: SQLiteDatabase?, connectionSource: ConnectionSource?, oldVersion: Int, newVersion: Int) {
		TableUtils.dropTable<StoreOrmliteEntity, Long>(connectionSource, StoreOrmliteEntity::class.java, false)
		TableUtils.dropTable<InstrumentOrmliteEntity, Long>(connectionSource, InstrumentOrmliteEntity::class.java, false)
		TableUtils.dropTable<InstrumentStockOrmliteEntity, Long>(connectionSource, InstrumentStockOrmliteEntity::class.java, false)
		onCreate(database, connectionSource)
	}
}