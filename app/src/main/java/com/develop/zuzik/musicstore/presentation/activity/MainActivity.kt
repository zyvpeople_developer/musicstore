package com.develop.zuzik.musicstore.presentation.activity

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.develop.zuzik.musicstore.R
import com.develop.zuzik.musicstore.application.extension.app
import com.develop.zuzik.musicstore.domain.entity.Store
import com.develop.zuzik.musicstore.domain.stores.Stores
import com.develop.zuzik.musicstore.presentation.extension.*
import com.develop.zuzik.musicstore.presentation.fragment.NoStoresFragment
import com.develop.zuzik.musicstore.presentation.map.activity.MapActivity
import com.develop.zuzik.musicstore.presentation.router.NullRouter
import com.develop.zuzik.musicstore.presentation.router.Router
import com.develop.zuzik.musicstore.presentation.store_detail.StoreDetailFragment
import com.develop.zuzik.musicstore.presentation.stores.StoresFragment
import com.develop.zuzik.musicstore.presentation.stores.StoresPresenter
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity :
		AppCompatActivity(),
		Router {

	private lateinit var routerStrategy: RouterStrategy

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_main)
		routerStrategy = if (phoneMode()) PhoneRouterStrategy() else TabletRouterStrategy()
		routerStrategy.onCreate(savedInstanceState)
		routerStrategy.displayFirstScreen()
	}

	override fun onStart() {
		super.onStart()
		routerStrategy.onStart()
	}

	override fun onStop() {
		routerStrategy.onStop()
		super.onStop()
	}

	override fun onSaveInstanceState(outState: Bundle?) {
		super.onSaveInstanceState(outState)
		routerStrategy.onSaveInstanceState(outState)
	}

	private fun phoneMode(): Boolean =
			fragmentContainer != null

	//region Router

	override fun navigateToStoreDetailScreen(store: Store) {
		routerStrategy.navigateToStoreDetailScreen(store)
	}

	override fun navigateToCallScreen(phone: String) {
		routerStrategy.navigateToCallScreen(phone)
	}

	override fun navigateToSendEmailScreen(email: String) {
		routerStrategy.navigateToSendEmailScreen(email)
	}

	override fun navigateToSiteScreen(site: String) {
		routerStrategy.navigateToSiteScreen(site)
	}

	override fun navigateToMapScreen() {
		routerStrategy.navigateToMapScreen()
	}

	//endregion

	//region RouterStrategy

	private interface RouterStrategy : Router {
		fun onCreate(savedInstanceState: Bundle?)
		fun onStart()
		fun onStop()
		fun onSaveInstanceState(outState: Bundle?)
		fun displayFirstScreen()
	}

	private inner abstract class BaseRouterStrategy : RouterStrategy {

		override fun onCreate(savedInstanceState: Bundle?) {
		}

		override fun onStart() {
		}

		override fun onStop() {
		}

		override fun onSaveInstanceState(outState: Bundle?) {
		}

		override fun navigateToCallScreen(phone: String) {
			showChooser(Intent().dialPhoneNumber(phone)) {
				showToast(R.string.error_no_application_to_dial_phone_number)
			}
		}

		override fun navigateToSendEmailScreen(email: String) {
			showChooser(Intent().sendEmail(email)) {
				showToast(R.string.error_no_application_to_send_email)
			}
		}

		override fun navigateToSiteScreen(site: String) {
			showChooser(Intent().openWebPage(site)) {
				showToast(R.string.error_no_application_to_open_site)
			}
		}

		override fun navigateToMapScreen() {
			startActivity(MapActivity.create(this@MainActivity))
		}
	}

	private inner class PhoneRouterStrategy : BaseRouterStrategy() {

		override fun displayFirstScreen() {
			if (supportFragmentManager.findFragmentById(R.id.fragmentContainer) == null) {
				supportFragmentManager
						.beginTransaction()
						.add(R.id.fragmentContainer, StoresFragment.create())
						.commit()
			}
		}

		override fun navigateToStoreDetailScreen(store: Store) {
			supportFragmentManager
					.beginTransaction()
					.replace(R.id.fragmentContainer, StoreDetailFragment.create(store))
					.addToBackStack(null)
					.commit()
		}
	}

	private inner class TabletRouterStrategy : BaseRouterStrategy() {

		private val CURRENT_SELECTED_STORE_SID = "CURRENT_SELECTED_STORE_SID"

		private var currentSelectedStoreSid: Long = 0
		private var presenter: Stores.Presenter? = null
		private var disposable: Disposable? = null

		override fun onCreate(savedInstanceState: Bundle?) {
			super.onCreate(savedInstanceState)
			currentSelectedStoreSid = savedInstanceState?.getLong(CURRENT_SELECTED_STORE_SID) ?: 0
		}

		override fun onStart() {
			super.onStart()
			val view = object : Stores.View {
				override val displayStores: PublishSubject<List<Store>> = PublishSubject.create()
				override val displayProgress: Observer<Boolean> = PublishSubject.create()
				override val displayError: Observer<String> = PublishSubject.create()
				override val onRefresh: Observable<Unit> = Observable.never()
				override val onStoreClicked: Observable<Store> = Observable.never()
				override val onMapClicked: Observable<Unit> = Observable.never()
			}
			disposable = view
					.displayStores
					.subscribe { stores ->
						if (stores.firstOrNull { it.sid == currentSelectedStoreSid } == null) {
							currentSelectedStoreSid = 0
							displayNoStoresFragment()
						}
					}

			val presenter = StoresPresenter(this@MainActivity.app.injection.applicationComponent, NullRouter())
			presenter.onStart(view)
		}

		override fun onStop() {
			presenter?.onStop()
			presenter = null
			disposable?.dispose()
			disposable = null
			super.onStop()
		}

		override fun onSaveInstanceState(outState: Bundle?) {
			super.onSaveInstanceState(outState)
			outState?.putLong(CURRENT_SELECTED_STORE_SID, currentSelectedStoreSid)
		}

		override fun displayFirstScreen() {
			if (supportFragmentManager.findFragmentById(R.id.masterFragmentContainer) == null) {
				supportFragmentManager
						.beginTransaction()
						.add(R.id.masterFragmentContainer, StoresFragment.create())
						.commit()
			}
			if (supportFragmentManager.findFragmentById(R.id.detailFragmentContainer) == null) {
				displayNoStoresFragment()
			}
		}

		override fun navigateToStoreDetailScreen(store: Store) {
			supportFragmentManager
					.beginTransaction()
					.replace(R.id.detailFragmentContainer, StoreDetailFragment.create(store))
					.commit()
		}

		private fun displayNoStoresFragment() {
			supportFragmentManager
					.beginTransaction()
					.replace(R.id.detailFragmentContainer, NoStoresFragment())
					.commit()
		}
	}

	//endregion
}
