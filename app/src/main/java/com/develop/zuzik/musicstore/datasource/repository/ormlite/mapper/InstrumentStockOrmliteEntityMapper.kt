package com.develop.zuzik.musicstore.datasource.repository.ormlite.mapper

import com.develop.zuzik.musicstore.datasource.repository.ormlite.entity.InstrumentOrmliteEntity
import com.develop.zuzik.musicstore.datasource.repository.ormlite.entity.InstrumentStockOrmliteEntity
import com.develop.zuzik.musicstore.domain.entity.InstrumentStock

/**
 * User: zuzik
 * Date: 3/25/17
 */
class InstrumentStockOrmliteEntityMapper : OrmliteEntityMapper<InstrumentStock, InstrumentStockOrmliteEntity> {

	override fun toEntity(ormliteEntity: InstrumentStockOrmliteEntity): InstrumentStock =
			InstrumentStock(
					id = ormliteEntity.id,
					quantity = ormliteEntity.quantity,
					instrumentId = ormliteEntity.instrument.id)

	override fun toOrmliteEntity(entity: InstrumentStock): InstrumentStockOrmliteEntity =
			InstrumentStockOrmliteEntity().apply {
				id = entity.id
				quantity = entity.quantity
				instrument = InstrumentOrmliteEntity().apply { id = entity.instrumentId }
			}
}