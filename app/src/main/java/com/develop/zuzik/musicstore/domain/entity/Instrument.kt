package com.develop.zuzik.musicstore.domain.entity

/**
 * User: zuzik
 * Date: 3/24/17
 */
data class Instrument(
		override val id: Long,
		override val sid: Long,
		val brand: String,
		val model: String,
		val type: InstrumentType,
		val price: Double,
		val storeId: Long) : ServerEntity