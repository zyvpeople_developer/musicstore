package com.develop.zuzik.musicstore.domain.store_detail

import com.develop.zuzik.musicstore.domain.entity.Instrument
import com.develop.zuzik.musicstore.domain.redux.model.ReduxModel
import io.reactivex.Observable
import io.reactivex.Observer

/**
 * User: zuzik
 * Date: 3/25/17
 */
interface StoreDetail {
	interface Model : ReduxModel<State> {
		val refresh: Observer<Any>
	}

	interface Presenter {
		fun onStart(view: View)
		fun onStop()
	}

	interface View {
		val displayStoreName: Observer<String>
		val displayStoreAddress: Observer<String>
		val displayTotalInstrumentsCount: Observer<Int>
		val displayInstruments: Observer<List<Instrument>>
		val displayProgress: Observer<Boolean>
		val displayError: Observer<String>
		val allowCall: Observer<Boolean>

		val onRefresh: Observable<Any>
		val onCall: Observable<Unit>
		val onSendEmail: Observable<Unit>
		val onOpenSite: Observable<Unit>
	}
}