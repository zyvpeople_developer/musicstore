package com.develop.zuzik.musicstore.datasource.network.mapper

import com.develop.zuzik.musicstore.datasource.network.response.InstrumentStockResponse
import com.develop.zuzik.musicstore.domain.entity.InstrumentStock

/**
 * User: zuzik
 * Date: 3/25/17
 */
class InstrumentStockResponseMapper : ResponseMapper<InstrumentStock, InstrumentStockResponse> {

	override fun toEntity(response: InstrumentStockResponse): InstrumentStock =
			InstrumentStock(
					id = 0,
					quantity = response.quantity,
					instrumentId = 0)
}