package com.develop.zuzik.musicstore.datasource.network.response

/**
 * User: zuzik
 * Date: 3/25/17
 */
data class StoreResponse(
		val id: Long,
		val name: String,
		val address: String,
		val phone: String?,
		val email: String,
		val size: String,
		val location: LocationResponse)