package com.develop.zuzik.musicstore.domain.entity

/**
 * User: zuzik
 * Date: 3/24/17
 */
data class InstrumentStock(
		override val id: Long,
		val quantity: Int,
		val instrumentId: Long) : Entity