package com.develop.zuzik.musicstore.domain.location.location_provider

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.util.Log
import com.develop.zuzik.musicstore.domain.location.location_provider.exception.LocationException
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.FusedLocationProviderApi
import com.google.android.gms.location.LocationListener
import com.google.android.gms.location.LocationRequest
import io.reactivex.Single
import io.reactivex.disposables.Disposable

/**
 * User: zuzik
 * Date: 3/26/17
 */

fun GoogleApiClient.connectSingle(): Single<GoogleApiClient> =
		Single.create { emitter ->
			val connectionCallbacks = object : GoogleApiClient.ConnectionCallbacks {
				override fun onConnected(bundle: Bundle?) {
					emitter.onSuccess(this@connectSingle)
				}

				override fun onConnectionSuspended(i: Int) {
					connect()
				}
			}
			val onConnectionFailedListener = GoogleApiClient.OnConnectionFailedListener {
				emitter.onError(LocationException.ConnectGooglePlayServices(it.errorMessage))
			}

			registerConnectionCallbacks(connectionCallbacks)
			registerConnectionFailedListener(onConnectionFailedListener)

			emitter.setDisposable(object : Disposable {
				override fun isDisposed(): Boolean =
						false

				override fun dispose() {
					unregisterConnectionCallbacks(connectionCallbacks)
					unregisterConnectionFailedListener(onConnectionFailedListener)
				}
			})

			connect()
		}

fun GoogleApiClient.locationObservable(
		context: Context,
		locationProviderAPI: FusedLocationProviderApi,
		locationRequest: LocationRequest): Single<Location> =
		Single.create { emitter ->
			val locationListener = LocationListener { location ->
				emitter.onSuccess(location)
			}
			if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
					&& ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
				emitter.onError(LocationException.LocationPermission())
			} else {
				emitter.setDisposable(object : Disposable {
					override fun isDisposed(): Boolean =
							false

					override fun dispose() {
						try {
							locationProviderAPI.removeLocationUpdates(this@locationObservable, locationListener)
						} catch (e: IllegalStateException) {
							Log.d(javaClass.simpleName, e.toString())
						}
					}
				})

				locationProviderAPI
						.requestLocationUpdates(
								this@locationObservable,
								locationRequest,
								locationListener)
			}
		}