package com.develop.zuzik.musicstore.datasource.repository.ormlite.repository

import com.develop.zuzik.musicstore.datasource.repository.exception.RepositoryException
import com.develop.zuzik.musicstore.datasource.repository.interfaces.ServerEntityRepository
import com.develop.zuzik.musicstore.datasource.repository.ormlite.entity.OrmliteServerEntity
import com.develop.zuzik.musicstore.datasource.repository.ormlite.mapper.OrmliteEntityMapper
import com.develop.zuzik.musicstore.domain.entity.ServerEntity
import com.j256.ormlite.dao.Dao

/**
 * User: zuzik
 * Date: 3/25/17
 */
open class OrmliteServerEntityRepository<E : ServerEntity, OrmliteE : OrmliteServerEntity>(
		dao: Dao<OrmliteE, Long>,
		mapper: OrmliteEntityMapper<E, OrmliteE>,
		setId: (Long, E) -> E) :
		OrmliteEntityRepository<E, OrmliteE>(dao, mapper, setId),
		ServerEntityRepository<E> {

	@Throws(RepositoryException.Read::class)
	override fun readWithSid(sid: Long): E {
		try {
			val ormliteEntity = dao.queryForEq(OrmliteServerEntity.SID, sid).firstOrNull() ?: throw RepositoryException.Read("Entity is not found")
			return mapper.toEntity(ormliteEntity)
		} catch (e: Exception) {
			throw RepositoryException.Read(e.message)
		}
	}

	@Throws(RepositoryException.Update::class)
	override fun updateWithSid(entity: E): E {
		try {
			val existedOrmliteEntity = readWithSid(entity.sid)
			val entityToUpdateWithId = setId(existedOrmliteEntity.id, entity)
			return updateWithId(entityToUpdateWithId)
		} catch (e: Exception) {
			throw RepositoryException.Update()
		}
	}

	@Throws(RepositoryException.Update::class)
	override fun createOrUpdateWithSid(entity: E): E =
			try {
				updateWithSid(entity)
			} catch (e: RepositoryException.Update) {
				try {
					create(entity)
				} catch (e: Exception) {
					throw RepositoryException.Update()
				}
			}
}