package com.develop.zuzik.musicstore.presentation.adapter.item.interfaces

import android.content.Context

/**
 * User: zuzik
 * Date: 3/24/17
 */
interface ItemViewFactory<Item> {
	fun create(context: Context?): ItemView<Item>
}