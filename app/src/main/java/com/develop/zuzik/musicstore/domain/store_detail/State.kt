package com.develop.zuzik.musicstore.domain.store_detail

import com.develop.zuzik.musicstore.domain.entity.Instrument
import com.develop.zuzik.musicstore.domain.entity.Store

/**
 * User: zuzik
 * Date: 3/25/17
 */
data class State(
		val store: Store?,
		val totalInstrumentsCount: Int,
		val instruments: List<Instrument>,
		val loading: Boolean)