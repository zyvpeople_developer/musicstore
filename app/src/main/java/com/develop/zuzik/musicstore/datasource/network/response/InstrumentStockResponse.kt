package com.develop.zuzik.musicstore.datasource.network.response

/**
 * User: zuzik
 * Date: 3/25/17
 */
data class InstrumentStockResponse(
		val instrument: InstrumentResponse,
		val quantity: Int)