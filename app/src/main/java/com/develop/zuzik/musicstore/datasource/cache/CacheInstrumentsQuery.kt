package com.develop.zuzik.musicstore.datasource.cache

import com.develop.zuzik.musicstore.datasource.repository.extension.createOrUpdateWithInstrumentIdObservable
import com.develop.zuzik.musicstore.datasource.repository.extension.createOrUpdateWithSidObservable
import com.develop.zuzik.musicstore.datasource.repository.extension.readInstrumentsForStoreObservable
import com.develop.zuzik.musicstore.datasource.repository.extension.readWithSidObservable
import com.develop.zuzik.musicstore.datasource.repository.interfaces.InstrumentRepository
import com.develop.zuzik.musicstore.datasource.repository.interfaces.InstrumentStockRepository
import com.develop.zuzik.musicstore.datasource.repository.interfaces.StoreRepository
import com.develop.zuzik.musicstore.domain.entity.Store
import com.develop.zuzik.musicstore.domain.store_detail.query.InstrumentQueryResult
import com.develop.zuzik.musicstore.domain.store_detail.query.InstrumentsQuery
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.functions.BiFunction

/**
 * User: zuzik
 * Date: 3/25/17
 */
class CacheInstrumentsQuery(private val instrumentsQuery: InstrumentsQuery,
							private val storeRepository: StoreRepository,
							private val instrumentRepository: InstrumentRepository,
							private val instrumentStockRepository: InstrumentStockRepository) : InstrumentsQuery {

	override fun load(storeSid: Long): Single<List<InstrumentQueryResult>> =
			CacheQuery(
					{ instrumentsQuery.load(storeSid) },
					{ results ->
						Observable
								.combineLatest<Store, InstrumentQueryResult, Pair<Store, InstrumentQueryResult>>(
										storeRepository
												.readWithSidObservable(storeSid)
												.toObservable(),
										Observable
												.fromIterable(results),
										BiFunction { store, instrumentQueryResult -> store to instrumentQueryResult })
								.flatMap { (store, instrumentQueryResult) ->
									val instrument = instrumentQueryResult.instrument.copy(storeId = store.id)
									instrumentRepository
											.createOrUpdateWithSidObservable(instrument)
											.flatMap { instrument ->
												val instrumentStock = instrumentQueryResult.instrumentStock.copy(instrumentId = instrument.id)
												instrumentStockRepository
														.createOrUpdateWithInstrumentIdObservable(instrumentStock)
														.map { instrumentStock ->
															InstrumentQueryResult(instrument, instrumentStock)
														}
											}
											.toObservable()
								}
								.toList()
					},
					{
						storeRepository
								.readWithSidObservable(storeSid)
								.flatMap {
									instrumentStockRepository
											.readInstrumentsForStoreObservable(it.id)
								}
					})
					.load()
}