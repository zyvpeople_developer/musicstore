package com.develop.zuzik.musicstore.domain.entity

/**
 * User: zuzik
 * Date: 3/25/17
 */
interface Entity {
	val id: Long
}