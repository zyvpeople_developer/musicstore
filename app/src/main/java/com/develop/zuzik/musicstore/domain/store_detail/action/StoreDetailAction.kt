package com.develop.zuzik.musicstore.domain.store_detail.action

import com.develop.zuzik.musicstore.domain.entity.Store
import com.develop.zuzik.musicstore.domain.redux.Action
import com.develop.zuzik.musicstore.domain.redux.model.ErrorAction
import com.develop.zuzik.musicstore.domain.store_detail.query.InstrumentQueryResult

/**
 * User: zuzik
 * Date: 3/25/17
 */
sealed class StoreDetailAction : Action {
	class BeginLoad : StoreDetailAction()
	class Load(val store: Store, val instrumentQueryResults: List<InstrumentQueryResult>) : StoreDetailAction()
	class Error(override val error: Throwable) : StoreDetailAction(), ErrorAction
}