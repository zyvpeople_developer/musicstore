package com.develop.zuzik.musicstore.domain.location.location_provider

import android.content.Context
import android.location.Location
import com.develop.zuzik.musicstore.domain.location.location_provider.connectSingle
import com.develop.zuzik.musicstore.domain.location.location_provider.locationObservable
import com.develop.zuzik.musicstore.domain.location.location_provider.availability.GooglePlayServicesAvailability
import com.develop.zuzik.musicstore.domain.location.location_provider.exception.LocationException
import com.develop.zuzik.musicstore.domain.location.location_provider.LocationProvider
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import io.reactivex.Single

/**
 * User: zuzik
 * Date: 3/26/17
 */
class GooglePlayServicesLocationProvider(private val context: Context,
										 private val locationRequest: LocationRequest) : LocationProvider {

	private val googlePlayServicesAvailability = GooglePlayServicesAvailability(context)

	override fun location(): Single<Location> =
			checkEnvironment()
					.flatMap { googleApiClient() }
					.flatMap { it.connectSingle() }
					.flatMap {
						it
								.locationObservable(
										context,
										LocationServices.FusedLocationApi,
										locationRequest)
								.doOnDispose { disconnect(it) }
					}

	private fun checkEnvironment(): Single<Any> =
			Single.defer {
				if (!googlePlayServicesAvailability.available()) {
					Single.error(LocationException.GooglePlayServicesNotAvailable())
				} else {
					Single.just(Any())
				}
			}

	private fun googleApiClient() = Single
			.defer {
				Single.just(GoogleApiClient.Builder(context)
						.addApi(LocationServices.API)
						.build())
			}

	private fun disconnect(googleApiClient: GoogleApiClient) {
		if (googleApiClient.isConnected || googleApiClient.isConnecting) {
			googleApiClient.disconnect()
		}
	}
}
